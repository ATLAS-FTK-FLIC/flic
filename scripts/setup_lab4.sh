#!/bin/bash

# Setup instructions based on Lab4 Twiki
#  https://twiki.cern.ch/twiki/bin/viewauth/Atlas/FastTrackerLab4Help#How_to_run_the_partition

. `dirname ${BASH_SOURCE[0]}`/setup.sh
export TDAQ_DB_DATA=${PWD}/daq/partitions/FTK.data.xml
export TDAQ_PARTITION=FTK_FLIConly
export TDAQ_DB_PATH=${PWD}/flic:${TDAQ_DB_PATH}

if [ "`echo ${PWD} | grep "/tbed/user/${USER}"`" == "" ]; then
    echo
    echo "  ERROR"
    echo "  (Ignore if you are just compiling)"
    echo "  If you are testing RC in Lab4, please make sure you run the partition from a directory"
    echo "  that is accessible by all the tbed nodes, e.g. /tbed/user/${USER}"
    echo
    return
fi

if [ ! -d installed ]; then
    echo 
    echo "  ERROR"
    echo "  (Ignore if you are just compiling)"
    echo "  To run the partition you will need to have the 'installed' directory"
    echo "  with compiled flic libraries (e.g. ReadoutModule_flic) inside this"
    echo "  directory. Please copy the 'installed' directory here or compile the code"
    echo "  using cmt, then re-run this setup script before starting the partition."
    echo
    return
fi

if [ ! -e ${TDAQ_DB_DATA} ]; then
    echo
    echo "  ERROR"
    echo "  (Ignore if you are just compiling)"
    echo "  Unable to find the FTK.data.xml partition file."
    echo "  Looked here:"
    echo "    ${TDAQ_DB_DATA}"
    echo
    return
fi

# Dump RepositoryRoot path in your database file
# (Should point to your installed directory with the FLIC readout module)
grep RepositoryRoot $TDAQ_DB_DATA

# Check the database
rc_checkapps -d oksconfig:${TDAQ_DB_DATA} -p ${TDAQ_PARTITION}

# Check partitions already running
ipc_ls -P -l

echo
echo "To start the partition:"
echo "  setup_daq -p $TDAQ_PARTITION"
echo "To check if it is open/closed:"
echo "  ipc_ls -P -l"
echo "To fully close (if there are lingering processes):"
echo "  pmg_kill_partition -p ${TDAQ_PARTITION}"
echo

echo "done."

