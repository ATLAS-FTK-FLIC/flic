#ifndef FLICMONITORINGFIFO_H
#define FLICMONITORINGFIFO_H

#include <stdint.h>

#include "ftkcommon/SpyBuffer.h"
#include "flic/flic_comm.h"

namespace daq { 
  namespace ftk {

    class FlicMonitoringFIFO : public daq::ftk::SpyBuffer
    {

    public:

      FlicMonitoringFIFO( ushort targetFpga , ushort fifoId , uint depth );
      ~FlicMonitoringFIFO();

      bool getSpyFreeze() { return false; }
      bool getSpyOverflow() { return false; }
      uint32_t getSpyPointer() { return 0; }
      uint32_t getSpyDimension() { return m_depth; } // monitoring fifos have max depth of 1024

      int read( daq::ftk::flic_comm *flic );
      
    private:

      ushort m_targetFpga;
      ushort m_fifoId;
      uint m_depth;
      
    };

  } // namespace ftk
} // namespace daq

#endif
