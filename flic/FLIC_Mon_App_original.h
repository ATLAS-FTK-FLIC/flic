/********************************************************/
/*    CREATED BY NEDAA                                  */
/********************************************************/

#ifndef FLIC_MON_APP_H
#define FLIC_MON_APP_H

#include <string>
#include <vector>


#include "ipc/partition.h"
#include "ipc/core.h"
#include "ROSCore/ReadoutModule.h"
#include "is/info.h"
#include "oh/OHRootProvider.h"
#include "flic/dal/flicNamed.h"
#include "flic/FLIC.h"
#include "flic/flic_comm.h"
#include "flic/FLICController.h"

// EMon
#include "ftkcommon/FtkEMonDataOut.h"

namespace daq {
  namespace ftk {

    class FLIC_Mon_App : public ROS::ReadoutModule
    {
    public:  // Constructor and destructor

      /// Constructor (NB: executed at CONFIGURE transition)
      FLIC_Mon_App();

      /// Destructor (NB: executed at UNCONFIGURE transition)
      virtual ~FLIC_Mon_App() noexcept;

    public: // overloaded methods inherited from ROS::ReadoutModule or ROS::IOMPlugin

      /// Set internal variables (NB: executed at CONFIGURE transition)
      virtual void setup(DFCountedPointer<ROS::Config> configuration) override;

      /// Reset internal statistics
      virtual void clearInfo() override;

      /// Get the list of channels connected to this ReadoutModule
      virtual const std::vector<ROS::DataChannel *> *channels() override;

      /// Set the Run Parameters
      //virtual void setRunConfiguration(DFCountedPointer<Config> runConfiguration);

      /// Get values of statistical counters
      // virtual DFCountedPointer<Config> getInfo();

      /// Get access to statistics stored in ISInfo class
      // virtual ISInfo* getISInfo();



    public: // overloaded methods inherited from RunControl::Controllable

      /// RC configure transition
      virtual void configure(const daq::rc::TransitionCmd& cmd) override;

      /// RC connect transition:
      virtual void connect(const daq::rc::TransitionCmd& cmd) override;

      /// RC start of run transition
      virtual void prepareForRun(const daq::rc::TransitionCmd& cmd) override;


      /// RC stop transition
      virtual void stopDC(const daq::rc::TransitionCmd& cmd) override;
      // virtual void stopROIB(const daq::rc::TransitionCmd& cmd) override;
      // virtual void stopHLT(const daq::rc::TransitionCmd& cmd) override;
      // virtual void stopRecording(const daq::rc::TransitionCmd& cmd) override;
      // virtual void stopGathering(const daq::rc::TransitionCmd& cmd) override;
      // virtual void stopArchiving(const daq::rc::TransitionCmd& cmd) override;

      /// RC unconfigure transition
      virtual void unconfigure(const daq::rc::TransitionCmd& cmd) override;

      /// RC disconnect transition:
      virtual void disconnect(const daq::rc::TransitionCmd& cmd) override;

      /// RC subtransition (IF NEEDED)
      // virtual void subTransition(const daq::rc::SubTransitionCmd& cmd) override;

      // Method called periodically by RC
      virtual void publish(void) override;

      /// RC HW re-synchronization request
      virtual void resynch(const daq::rc::ResynchCmd& cmd);

    private:

      flic_comm *m_flic;
      FLICController m_ctrl;

      std::vector<ROS::DataChannel *>  m_dataChannels;
      DFCountedPointer<ROS::Config>      m_configuration; /**< Configuration Object, Map Wrapper */
      std::string                                                  m_isServerName;      /**< IS Server  */
      IPCPartition                                                 m_ipcpartition;      /**< Partition  */
      uint32_t                                                     m_runNumber;                 /**< Run Number */
      bool                             m_dryRun;        ///< Bypass VME calls
      bool                             m_isLoadMode;    ///< Temp variable to keep track of daq::ftk::isLoadMode
      ::ftk::flicNamed                *m_flicNamed;     ///< Access IS via schema
      OHRootProvider                  *m_ohProvider;    ///< Histogram provider

      // FLIC BOARD Specific Parameters
      std::string     m_name;                           /**< Card name  */
      uint32_t        m_slot;                           /**< Slot       */
      uint32_t        m_boardNumber;                    /**< ID for the board */
      std::string     m_flicip;                         /**< IP of FLIC */
      std::string     m_port;                           /**< FLIC port  */
      std::string     m_hostip;                         /**< IP of Host */
      bool            m_loopbackMode;                   /**< Loopback Mode Toggle */
      bool            m_ignoreROSLinks;                 /**< Dump-data-on-the-floor Toggle */

      // EMon
      daq::ftk::FtkEMonDataOut *m_ftkemonDataOut;       /**< Pointer to FtkEMonDataOut for publishing spybuffers */

      // Histograms
      TH1F           *m_histogram;

    };

    inline const std::vector<ROS::DataChannel *> *FLIC_Mon_App::channels()
    {
      return &m_dataChannels;
    }

  } // namespace ftk
} // namespace daq
#endif // FLIC_MON_APP_H
