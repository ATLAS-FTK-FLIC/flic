/*****************************************************************/
/*        Created by Nedaa                                       */
/*                                                               */
/*****************************************************************/

//#include <iostream>
#include <string>

// OKS, RC and IS
#include "flic/FLIC_Mon_App.h"
#include "flic/dal/FLIC_Mon.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"

// Common FTK tools
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

// VME
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

// Emon
#include "flic/FlicMonitoringFIFO.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include <memory> // shared_ptr
#include <utility> // pair

using namespace daq::ftk;
using namespace std;

/**********************/
FLIC_Mon_App::FLIC_Mon_App()
/**********************/
{
  #include "cmt/version.ixx"
  ERS_LOG("FLIC_Mon_App::constructor: Entered"      << std::endl
    << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl
    << cmtversion );

  m_configuration = 0;
  m_dryRun        = false;

  // JW - Need to add soft reset for this function
  m_isLoadMode = daq::ftk::isLoadMode();
  if( m_isLoadMode ) ERS_LOG( "isLoadMode is True" );

  ERS_LOG("FLIC_Mon_App::constructor: Done");
}

/***********************/
FLIC_Mon_App::~FLIC_Mon_App() noexcept
/***********************/
{
  ERS_LOG("FLIC_Mon_App::destructor: Entered");
  delete m_flic;
  ERS_LOG("FLIC_Mon_App::destructor: Done");
}

/************************************************************/
void FLIC_Mon_App::setup(DFCountedPointer<ROS::Config> configuration)
/************************************************************/
{
  ERS_LOG("FLIC_Mon_App::setup: Entered");

  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  std::string appName = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

  // Read some info from the DB
  const ftk::dal::FLIC_Mon * module_dal = conf->get<ftk::dal::FLIC_Mon>(configuration->getString("UID"));
  m_name	   = module_dal->get_Name();
  m_slot	   = module_dal->get_Slot();
  m_boardNumber    = module_dal->get_BoardNumber();
  m_flicip	   = module_dal->get_FlicIP();
  m_port	   = module_dal->get_Port();
  m_hostip	   = module_dal->get_HostIP();
  m_loopbackMode   = module_dal->get_LoopbackMode();
  m_ignoreROSLinks = module_dal->get_IgnoreROSLinks();
  m_isServerName   = readOutConfig->get_ISServerName();
  m_dryRun	   = module_dal->get_DryRun();
  if(m_dryRun)
    { FTK_WARNING("FLIC_Mon.DryRun option set in DB, IPBus calls with be skipped!" ) };

  // Initialize flic_comm object for communicating with board
  // using the IP information included in the segment DB
  // (Readout_flic.schema.xml attributes)
  m_flic = new flic_comm( m_flicip.c_str() , m_port.c_str() , m_hostip.c_str() );

  // Set the FLIC controller in loopback mode if specified by user
  m_ctrl.setLoopbackMode( m_loopbackMode );
  m_ctrl.setDumpDataOnTheFloor( m_ignoreROSLinks );
  
  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try { m_flicNamed = new ::ftk::flicNamed( m_ipcpartition, isProvider.c_str() ); }
  catch( ers::Issue &ex)
    {
      daq::ftk::ISException e(ERS_HERE, "Error while creating flicNamed object", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try {
    m_ohProvider = new OHRootProvider ( m_ipcpartition , OHServer, OHName );
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << appName);
  }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception.
      daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Construct the histograms
  m_histogram = new TH1F("newHistogram","newRegHistogram",10,-50.5,50.5);

  // Tell PIC to power the FPGAs on the board
  m_ctrl.setup( m_flic );

  ERS_LOG("FLIC_Mon_App::setup: Done");
}

unsigned int countBits(unsigned int data) {
  int count=0;
  for (unsigned int i=0; i<sizeof(unsigned int)*8; i++)
    if ( (data>>i)&0x1 ) count++;
  return count;
}

/********************************/
void FLIC_Mon_App::configure(const daq::rc::TransitionCmd& cmd)
/********************************/
{
  ERS_LOG(cmd.toString());
  m_ctrl.configure( m_flic );
  ERS_LOG(cmd.toString() << " Done");
}

/**************************/
void FLIC_Mon_App::unconfigure(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void FLIC_Mon_App::connect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  // try to initialize EMon data handler
  m_ftkemonDataOut = reinterpret_cast<daq::ftk::FtkEMonDataOut*>(ROS::DataOut::sampled());
  if( !m_ftkemonDataOut ) {
    FTK_WARNING("EMonDataOut plugin not found! SpyBuffer data will not be published in emon");
  } else {
    m_ftkemonDataOut->setScalingFactor(1);
    ERS_LOG("Found EMonDataOut plugin");
  }

  m_ctrl.connect( m_flic );
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void FLIC_Mon_App::disconnect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}



/**********************************/
void FLIC_Mon_App::prepareForRun(const daq::rc::TransitionCmd& cmd)
/**********************************/
{
  ERS_LOG(cmd.toString());
  m_ctrl.prepareForRun( m_flic );
  ERS_LOG(cmd.toString() << " Done");
}


/**************************/
void FLIC_Mon_App::stopDC(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  m_ctrl.stopDC( m_flic );
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void FLIC_Mon_App::publish()
/**************************/
{
  ERS_LOG("Publishing...");

  // Collect the relevant information from VME
  // TODO: fill

  // Publish in IS with schema, i.e. fill ISInfo object

  // PIC registers
  m_flicNamed->IPMCPowerStat   = m_ctrl.GetRegister( m_flic , FLIC::targetMgmtFpga , 0x0 );
  m_flicNamed->PowerControlMod = m_ctrl.GetRegister( m_flic , FLIC::targetMgmtFpga , 0x2 );

  // Other attributes to be monitored in IS
  // Resize

  // XOFF counters
  m_flicNamed->Pipeline0.resize(4);
  m_flicNamed->SSB_XOFF.resize(4);
  m_flicNamed->SFP_FIFO.resize(4);
  m_flicNamed->CCMUX_FIFO.resize(4);
  m_flicNamed->SRAM_FIFO.resize(4);
  m_flicNamed->MERGE_FIFO.resize(4);
  m_flicNamed->RTM_FIFO.resize(4);

  // GLOBAL status words
  m_flicNamed->GLOBAL_STATUS.resize(4);
  
  // Pipeline counters
  m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT.resize(4);
  m_flicNamed->NOT_IN_SYNC_COUNT.resize(4);
  m_flicNamed->HEADER_ERROR_COUNT.resize(4);
  m_flicNamed->TRACK_ERROR_COUNT.resize(4);
  m_flicNamed->TRAILER_ERROR_COUNT.resize(4);
  m_flicNamed->TRUNCATION_ERROR_COUNT.resize(4);
  m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT.resize(4);
  m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT.resize(4);
  m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT.resize(4);
  m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT.resize(4);
  m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT.resize(4);
  m_flicNamed->RTM_FIFO_EVENT_END_COUNT.resize(4);
  m_flicNamed->FLIC_FLOW_CTL.resize(4);
  m_flicNamed->L1_ID_MATCH_U3_COUNT.resize(4);
  m_flicNamed->L1_ID_MATCH_U4_COUNT.resize(4);

  // Monitoring fifos
  m_flicNamed->MonSlice0.resize(10);
  m_flicNamed->MonSlice1.resize(10);
  m_flicNamed->MonSlice2.resize(10);
  m_flicNamed->MonSlice3.resize(10);
  
  // We need to still decide on this one
  // This is under development 
  m_flicNamed->Pipeline0[0] = m_ctrl.GetRegister( m_flic , FLIC::targetFpgaU1 , 0x0105 ); // CCMUX_STATUS_REG

  // XOFFs from various parts of the pipeline
  ushort xoff_counts[28];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x0124 , xoff_counts , 28 );
  for( ushort i = 0 ; i < 4 ; i++ ) {
    // Loop over the four channels in U1
    m_flicNamed->SSB_XOFF[i]   = xoff_counts[0+i]; // SSB_XOFF
    m_flicNamed->SFP_FIFO[i]   = xoff_counts[4+i]; // SFP_FIFO
    m_flicNamed->CCMUX_FIFO[i] = xoff_counts[8+i]; // CCMUX_FIFO
    m_flicNamed->SRAM_FIFO[i]  = xoff_counts[12+i]; // SRAM_FIFO
    m_flicNamed->MERGE_FIFO[i] = xoff_counts[16+i]; // MERGE_FIFO
    m_flicNamed->RTM_FIFO[i]   = xoff_counts[18+i]; // RTM_FIFO

    // The global status word is read from registers 0x13C - 0x13F, immediately after the
    // XOFF counts. We grab it in the same block read to save time.
    m_flicNamed->GLOBAL_STATUS[i] = xoff_counts[24+i]; // GLOBAL STATUS words

    // Status definitions:
    //   FLIC_GLOBAL_STATUSes(i)(0) <=  SFP_FIFO_PROG_FULLs(i);		--	16: SFP input FIFO Prog Full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(1) <=  CCMUX_FIFO_PROG_FULLs(i);	--	17: CCMUX FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(2) <=  SRAM_FIFO_PROG_FULLs(i);	-- 18: SRAM Lookup FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(3) <=  MERGE_FIFO_PROG_FULLs(i);	-- 19: Merge FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(4) <=  RTM_FIFO_PROG_FULLs(i);	-- 20: RTM FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(5) <=  COLLECTED_PIPELINE_PROG_FULLs(i)(5);	-- 21: User test Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(6) <=  U3_TAGGED_EVENT_FIFO_PROG_FULLs(i);	-- 22: Spy buffer Tag FIFO #1 Prog full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(7) <=  U4_TAGGED_EVENT_FIFO_PROG_FULLs(i);	-- 23: Spy buffer Tag FIFO #2 Prog full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(8) <=  SLINK_LDOWN_FLAGs(i);	-- 24: SLINK went down since last event was transmitted
    //   FLIC_GLOBAL_STATUSes(i)(9) <=  SLINK_LFF_FLAGs(i);	-- 25: SLINK FIFO FULL  (LFF)
    //   FLIC_GLOBAL_STATUSes(i)(10) <=  SLINK_XOFF_FLAGs(i);	-- 26: SLINK XOFF
    //   FLIC_GLOBAL_STATUSes(i)(11) <=  NOT_IN_SYNCs(i);	-- 27: NOT_IN_SYNC (Core Crate Receiver not synchronized to input)
    //   FLIC_GLOBAL_STATUSes(i)(12) <=  SLINK_CTL2_REG(i+4);	-- 28: User Defined status bit 1
    //   FLIC_GLOBAL_STATUSes(i)(13) <=  SLINK_CTL2_REG(i+8);	-- 29: User Defined status bit 2
    //   FLIC_GLOBAL_STATUSes(i)(14) <=  '0';	-- 30: Reserved for use as enable/disable of checking the STAT FIFOS
    //   FLIC_GLOBAL_STATUSes(i)(15) <=  '0';	-- 31: Reserved
    
    
  }

  // Clock frequencies
  ushort clock_counts_u[2][8];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x0106 , clock_counts_u[0] , 8 );
  //m_flic->blockread( FLIC::targetFpgaU2 , FLIC::cmdArrayRead , 0x0106 , clock_counts_u[1] , 8 );
  for( ushort i = 0 ; i < 2 ; i++ ) {
    // Loop over U1 and U2
    // (The U2 counts are not read for the moment since the SSBe firmware is being loaded)
    m_flicNamed->SRAM_REFCLK[i]   = clock_counts_u[i][0];
    m_flicNamed->DDR_REFCLK[i]    = clock_counts_u[i][1];
    m_flicNamed->GTX112_REFCLK[i] = clock_counts_u[i][2];
    m_flicNamed->GTX112_TXCLK[i]  = clock_counts_u[i][3];
    m_flicNamed->GTX112_RXCLK[i]  = clock_counts_u[i][4];
    m_flicNamed->GTX116_REFCLK[i] = clock_counts_u[i][5];
    m_flicNamed->GTX116_TXCLK[i]  = clock_counts_u[i][6];
    m_flicNamed->GTX116_RXCLK[i]  = clock_counts_u[i][7];
  }


  // Processor counters
  for( ushort i = 0 ; i < 4 ; i++ ) {
    ushort proc_counts[0x10];
    m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x310+(i*0x10) , proc_counts , 0x10 );
    m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT[i]   = proc_counts[0x0];
    m_flicNamed->NOT_IN_SYNC_COUNT[i]	        = proc_counts[0x1];
    m_flicNamed->HEADER_ERROR_COUNT[i]	        = proc_counts[0x2];
    m_flicNamed->TRACK_ERROR_COUNT[i]	        = proc_counts[0x3];
    m_flicNamed->TRAILER_ERROR_COUNT[i]	        = proc_counts[0x4];
    m_flicNamed->TRUNCATION_ERROR_COUNT[i]      = proc_counts[0x5];
    m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[i]  = proc_counts[0x6];
    m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT[i]  = proc_counts[0x7];
    m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT[i] = proc_counts[0x8];
    m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT[i]  = proc_counts[0x9];
    m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT[i]  = proc_counts[0xa];
    m_flicNamed->RTM_FIFO_EVENT_END_COUNT[i]    = proc_counts[0xb];
    m_flicNamed->FLIC_FLOW_CTL[i]	        = proc_counts[0xc];
    m_flicNamed->L1_ID_MATCH_U3_COUNT[i]        = proc_counts[0xd];
    m_flicNamed->L1_ID_MATCH_U4_COUNT[i]        = proc_counts[0xe];
    // last counter currently unused
  }
  
  // Monitoring FIFOs
  vector< pair< shared_ptr<SpyBuffer> , uint32_t > > vec_MonFifos;
  for( ushort imon = 0 ; imon < 4 /*Number of FIFOs in a single pipeline*/ ; imon++ ) {

    shared_ptr<FlicMonitoringFIFO> monFifo = make_shared< FlicMonitoringFIFO>( FLIC::targetFpgaU1 , imon , 256 );
    monFifo->read( m_flic );
    uint32_t sourceId = encode_SourceIDSpyBuffer( BoardType::Flic , m_boardNumber , imon , Position::OTHER_POSITION );
    vec_MonFifos.push_back( make_pair(monFifo,sourceId) );

    // Put the first 10 words from each fifo in IS
    for( ushort iword = 0 ; iword < 10 ; iword++ ) {
      if( imon==0 )      m_flicNamed->MonSlice0[iword] = monFifo->getBuffer()[iword];
      else if( imon==1 ) m_flicNamed->MonSlice1[iword] = monFifo->getBuffer()[iword];
      else if( imon==2 ) m_flicNamed->MonSlice2[iword] = monFifo->getBuffer()[iword];
      else if( imon==3 ) m_flicNamed->MonSlice3[iword] = monFifo->getBuffer()[iword];
    }

    // Scan MonFifo0 for an example RunNumber and Lvl1ID to put in IS
    /*
    if( imon==0 ) {
      for( ushort iword = 0 ; iword < 256 - 7 ; iword++ ) {
	if( monFifo->getBuffer()[iword] != 0xB0F0 ) continue;
	if( monFifo->getBuffer()[iword+1] != 0xCAF0 ) continue;
	if( monFifo->getBuffer()[iword+2] != 0xFF12 ) continue;
	if( monFifo->getBuffer()[iword+3] != 0xFF34 ) continue;
	break;
      }
    }
    */
  }


  // Arm MonFifos for next call to publish
  m_ctrl.ArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 ); 

  // Ship Mon FIFO data to EMon
  if( m_ftkemonDataOut ) {
    ERS_LOG( "Shipping Monitoring FIFO data to EMonDataOut ..." );
    m_ftkemonDataOut->sendData( vec_MonFifos );
    ERS_LOG( "Shipment complete" );
  }

 
  // Check in the IS info
  try { m_flicNamed->checkin(); }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception.
      daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Fill and publish histograms
  // Example:
  m_histogram->Fill( rand()%100 - 50 );
  try { m_ohProvider->publish( *m_histogram, "newRegHistogram", true ); }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception.
      daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  //
  // Before ending publish, do some "stopless removal" checks...
  // FIXME!
  //
  // Check that ROS links are up, i.e. that LDOWN = 0 on each channel
  //if( (m_flicNamed->GLOBAL_STATUS[0] >> 8) & 1 ) {
  //  daq::rc::HardwareError e( ERS_HERE , "FTK_Dummy_Busy_Channel" , ex );
  //  ers::error( e );
  //}
  
}

/**************************/
void FLIC_Mon_App::resynch(const daq::rc::ResynchCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  m_ctrl.resynch( m_flic );
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void FLIC_Mon_App::clearInfo()
/**************************/
{
  ERS_LOG("Clear histograms and counters");

  // Reset histograms

  m_histogram->Reset();

  // Reset the IS counters

  m_flicNamed->IPMCPowerStat   = 0 ;
  m_flicNamed->PowerControlMod = 0 ;
  
  m_flicNamed->Pipeline0[0] = 0 ;

  for( ushort i = 0 ; i < 4 ; i++ ) {
    // Loop over the four pipelines in U1
    m_flicNamed->SSB_XOFF[i]	  = 0 ;
    m_flicNamed->SFP_FIFO[i]	  = 0 ;
    m_flicNamed->CCMUX_FIFO[i]	  = 0 ;
    m_flicNamed->SRAM_FIFO[i]	  = 0 ;
    m_flicNamed->MERGE_FIFO[i]	  = 0 ;
    m_flicNamed->RTM_FIFO[i]	  = 0 ;
    m_flicNamed->GLOBAL_STATUS[i] = 0 ;
  }

  for( ushort i = 0 ; i < 2 ; i++ ) {
    // Loop over U1 and U2
    m_flicNamed->SRAM_REFCLK[i]   = 0 ;
    m_flicNamed->DDR_REFCLK[i]    = 0 ;
    m_flicNamed->GTX112_REFCLK[i] = 0 ;
    m_flicNamed->GTX112_TXCLK[i]  = 0 ;
    m_flicNamed->GTX112_RXCLK[i]  = 0 ;
    m_flicNamed->GTX116_REFCLK[i] = 0 ;
    m_flicNamed->GTX116_TXCLK[i]  = 0 ;
    m_flicNamed->GTX116_RXCLK[i]  = 0 ;
  }

  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT[i]   = 0 ;
    m_flicNamed->NOT_IN_SYNC_COUNT[i]	        = 0 ;
    m_flicNamed->HEADER_ERROR_COUNT[i]	        = 0 ;
    m_flicNamed->TRACK_ERROR_COUNT[i]	        = 0 ;
    m_flicNamed->TRAILER_ERROR_COUNT[i]	        = 0 ;
    m_flicNamed->TRUNCATION_ERROR_COUNT[i]      = 0 ;
    m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[i]  = 0 ;
    m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT[i]  = 0 ;
    m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT[i] = 0 ;
    m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT[i]  = 0 ;
    m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT[i]  = 0 ;
    m_flicNamed->RTM_FIFO_EVENT_END_COUNT[i]    = 0 ;
    m_flicNamed->FLIC_FLOW_CTL[i]	        = 0 ;
    m_flicNamed->L1_ID_MATCH_U3_COUNT[i]        = 0 ;
    m_flicNamed->L1_ID_MATCH_U4_COUNT[i]        = 0 ;
    // last counter currently unused
  }

  for( ushort i = 0 ; i < 10 ; i++ ) {
    m_flicNamed->MonSlice0[i] = 0 ;
    m_flicNamed->MonSlice1[i] = 0 ;
    m_flicNamed->MonSlice2[i] = 0 ;
    m_flicNamed->MonSlice3[i] = 0 ;
  }

}


//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createFLIC_Mon_App();
}

ROS::ReadoutModule* createFLIC_Mon_App()
{
  return (new FLIC_Mon_App());
}

