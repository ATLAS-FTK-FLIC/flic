#include "../flic/FLIC_Mon_App.h"

FLIC_Mon_App::FLIC_Mon_App() {
}

void FLIC_Mon_App::setup() {

  flic_udp_buffer.resize(100);
  //events_position.resize(1);
  //  events_position.at(0).ET01_position.resize(10);
  
  //////////////////// malloc buffer space in memory
  //  events = new std::vector<struct one_event> (SPYBUFFERSIZE);

  /*
  std::cout << events->size() << std::endl;
  for ( auto &evt : *events) {
    evt.half_event_from_eth7 = new std::deque<FLIC_UDP_packet> (HALFEVENTSIZE);
    std::cout << evt.half_event_from_eth7 << std::endl;
    std::cout << evt.half_event_from_eth7->size() << std::endl;
  }
  */
  
  //////////////////// setup blade socket and address
  blade_ETH_sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  memset(&blade_ETH_addr, 0, sizeof(blade_ETH_addr));
  blade_ETH_addr.sin_family = AF_INET;
  blade_ETH_addr.sin_addr.s_addr = htonl(INADDR_ANY);
  blade_ETH_addr.sin_port = htons(50001);  // SERV_PORT);
  bind(blade_ETH_sockfd, (struct sockaddr *) &blade_ETH_addr, sizeof(blade_ETH_addr));

  //////////////////// setup FLIC GTX socket and address
  memset(&FLIC1_Spybuffer_addr, 0, sizeof(FLIC1_Spybuffer_addr));
  FLIC1_Spybuffer_addr.sin_family = AF_INET;
  inet_pton(AF_INET, "192.168.2.2", &FLIC1_Spybuffer_addr.sin_addr);
  FLIC1_Spybuffer_addr.sin_port = htons(50000);   // CLI_PORT);
  addr_ptr = (struct sockaddr *) &FLIC1_Spybuffer_addr;
  addr_len = sizeof(FLIC1_Spybuffer_addr);
     
  //////////////////// setup histograms
  m_hTrackChi2_Blade = new TH1F( "hTrackChi2" , "TrackChi2;#chi^{2};Monitored Ev" , 25 , 0 , 500 );
  m_hTrackD0_Blade = new TH1F( "hTrackD0" , "TrackD0;d_{0} [mm];Monitored Ev" , 30 , -3.0 , 3.0 );
  m_hTrackZ0_Blade = new TH1F( "hTrackZ0" , "TrackZ0;z_{0} [mm];Monitored Ev" , 40 , -200.0 , 200.0 );
  m_hTrackCotth_Blade = new TH1F( "hTrackCotth" , "TrackCotth;cot#theta;Monitored Ev" , 30 , -6 , 6 );
  m_hTrackPhi_Blade = new TH1F( "hTrackPhi" , "TrackPhi;#phi_{0};Monitored Ev" , 32 , -8 , 8 );
  m_hTrackCurv_Blade = new TH1F( "hTrackCurv" , "TrackCurv;1/2*p_{T} [GeV^{-1}];Monitored Ev" , 25 , -0.5 , 0.5 );

}



void FLIC_Mon_App::prepareForRun() {
  int i = 0;
  int j = 0;
  int n;

  bool previous_udp_packet_full = false;
  bool prev_header_exist = false;

  std::deque<assembly_fragment_position>::iterator itr_temp;
  
  for (i=0; ; ++i) {
    i = i % flic_udp_buffer.size();

    n = recvfrom(blade_ETH_sockfd, &(flic_udp_buffer.at(i).mesg), MAXLINE, 0, addr_ptr, &addr_len);
    
    if ( !previous_udp_packet_full ) {
      events_position.push_back( *(new assembly_fragment_position ()) ); // then this is the head of the event
      events_position.back().beginning_position = flic_udp_buffer.at(i).mesg;

      // if previous record header existing
      itr_temp = events_position.end();
      if ( prev_header_exist ) {
        --itr_temp;
        mark_ET01_position(itr_temp , itr_temp->beginning_position,  events_position.back().beginning_position );
        // find the tag header between previous and the back()

        // check if there are errors
        checkEvent(itr_temp);

      }else {
        prev_header_exist = true;
        continue;
      }
      
        
    }
    
  }    


}

  /*  
  for (i=0; ; ++i) {
    i = i % flic_udp_buffer.size();
    
    n = 1500;
    events->at(i).length_half_event_from_eth7 = 0;
    events->at(i).half_event_from_eth7->clear();
    events->at(i).event_tagging_information_positions->clear();
    
    for (j=0; n == 1500 ; ++j) {
      n = recvfrom(blade_ETH_sockfd, &mesg_buffer, MAXLINE, 0,  addr_ptr, &addr_len);

      events->at(i).half_event_from_eth7->push_back(mesg_buffer);
      
      events->at(i).length_half_event_from_eth7 += n; // will this be bytes or others?
    }

    mark_ET01_position(i);
  }
  
}
  

void FLIC_Mon_App::prepareForRun() {
  int i;
  int j;
  int n; // return message length by "recvfrom"
  
  for (i=0; ; ++i) {
    i = i % SPYBUFFERSIZE;

    n = 0;
    events[i].length_half_event_from_eth7 = 0;
    
    for (j=0; n >= 1500 ; ++j) {
      n = recvfrom(blade_ETH_sockfd, (events[i].half_event_from_eth7 + j), MAXLINE, 0,  addr_ptr, &addr_len);

      events[i].length_half_event_from_eth7 += n; // will this be bytes or others?
    }

    ////////// check data after receiving a full event
    checkData(i);
  }

}
  */



int FLIC_Mon_App::mark_ET01_position(std::deque<assembly_fragment_position>::iterator evt, uint16_t * prev_header_ptr, uint16_t * back_header_ptr) {
  // mark end position of the event
  prev_header_ptr = prev_header_ptr + 1;
  evt->end_position = prev_header_ptr + *prev_header_ptr;
  
  // mark the first ET01 position
  prev_header_ptr = prev_header_ptr + 5;

  for ( ; ; ) {
    evt->ET01_position.push_back(prev_header_ptr);

    // read the length of the debug block
    prev_header_ptr = prev_header_ptr + 40;
    if ( !( *prev_header_ptr == *(prev_header_ptr + *prev_header_ptr) ) )
      std::cout << " throw some exceptions" << std::endl;
    else {
      prev_header_ptr = prev_header_ptr + *prev_header_ptr;
    }

    if ( (evt->end_position - prev_header_ptr) <= 11 ) {
      std::cout << " this is the end of the ET01 tag" << std::endl;
      // return something
      return 0;
    }

    prev_header_ptr = prev_header_ptr + 9;
  }

  
  /*
  // loop deque
  for (auto &udp_packet :  *(events->at(i).half_event_from_eth7) )
    if ( events->at(i).half_event_from_eth7->at(0).mesg[5] == 48640 ) // hex. be00 == dec. 48640 
      push_back;
  then mesg = mesg + 45; // line "length of debug block"
  then mesg = mesg + N ; // end of "length of debug block"
  then mesg = mesg + 9; // this should be next ET01

  if (udp_packet.mesg[
          events->at(i).event_tagging_information_positions->push_back( &(events->at(i).half_event_from_eth7->at(j).mesg[5]) );
  
          }
  
      }

*/  
  //return 0;
}


int FLIC_Mon_App::checkEvent( std::deque<assembly_fragment_position>::iterator evt ) {
  
  
  return 0;
}




FLIC_Mon_App::~FLIC_Mon_App() {

}
