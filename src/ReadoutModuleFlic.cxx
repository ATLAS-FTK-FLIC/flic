/*****************************************************************/
/*                                                               */
/*                                                               */
/*****************************************************************/

//#include <iostream>
#include <string>

// OKS, RC and IS
#include "flic/ReadoutModuleFlic.h"
#include "flic/dal/FTK_RCD_AtcaCrate_FlicNamed.h"
#include "flic/dal/ReadoutModule_flic.h"
#include "config/Configuration.h"
#include "DFdal/RCD.h"
#include "DFdal/ReadoutConfiguration.h"
#include "RunControl/Common/OnlineServices.h"
#include "is/info.h"

// To include daq::rc::HardwareError
#include "ers/ers.h"
#include "RunControl/Common/UserExceptions.h"

// Common FTK tools
#include "ftkcommon/exceptions.h"
#include "ftkcommon/core.h"

// VME
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

// Emon
#include "flic/FlicMonitoringFIFO.h"
#include "ftkcommon/SourceIDSpyBuffer.h"
#include <memory> // shared_ptr
#include <utility> // pair

using namespace daq::ftk;
using namespace std;

/**********************/
ReadoutModule_flic::ReadoutModule_flic()
/**********************/
{
#include "cmt/version.ixx"
  ERS_LOG("ReadoutModule_flic::constructor: Entered"      << std::endl
          << "Build timestamp: " << __DATE__ << " " << __TIME__ << std::endl
          << cmtversion );

  m_configuration = 0;
  m_dryRun        = false;

  // JW - Need to add soft reset for this function
  m_isLoadMode = daq::ftk::isLoadMode();
  if( m_isLoadMode ) ERS_LOG( "isLoadMode is True" );

  ERS_LOG("ReadoutModule_flic::constructor: Done");
}

/***********************/
ReadoutModule_flic::~ReadoutModule_flic() noexcept
/***********************/
{
  ERS_LOG("ReadoutModule_flic::destructor: Entered");
  delete m_flic;
  ERS_LOG("ReadoutModule_flic::destructor: Done");
}

/************************************************************/
void ReadoutModule_flic::setup(DFCountedPointer<ROS::Config> configuration)
/************************************************************/
{
  ERS_LOG("ReadoutModule_flic::setup: Entered");

  // Get online objects from daq::rc::OnlineServices
  m_ipcpartition      = daq::rc::OnlineServices::instance().getIPCPartition();
  m_appName	      = daq::rc::OnlineServices::instance().applicationName();
  auto dal_rcdAppl    = daq::rc::OnlineServices::instance().getApplication().cast<daq::df::RCD>();
  Configuration *conf = configuration->getPointer<Configuration>("configurationDB");
  // OR: Configuration &conf   = daq::rc::OnlineServices::instance().getConfiguration();
  auto readOutConfig  = conf->cast<daq::df::ReadoutConfiguration>( dal_rcdAppl->get_Configuration() );

  // Read some info from the DB
  const ftk::dal::ReadoutModule_flic * module_dal = conf->get<ftk::dal::ReadoutModule_flic>(configuration->getString("UID"));
  m_name		     = module_dal->get_Name();
  m_slot		     = module_dal->get_Slot();
  m_boardNumber		     = module_dal->get_BoardNumber();
  m_flicip		     = module_dal->get_FlicIP();
  m_port		     = module_dal->get_Port();
  m_hostip		     = module_dal->get_HostIP();
  m_verbose		     = module_dal->get_Verbose();
  m_loopbackMode	     = module_dal->get_LoopbackMode();
  m_useAsSSBe                = module_dal->get_UseAsSSBe();
  m_tracksPerRecord	     = module_dal->get_TracksPerRecord();
  m_delayBetweenRecords	     = module_dal->get_DelayBetweenRecords();
  m_totalRecords             = module_dal->get_TotalRecords();
  m_delayBetweenSyncAttempts = module_dal->get_DelayBetweenSyncAttempts();
  m_totalSyncAttempts	     = module_dal->get_TotalSyncAttempts();
  m_linksInUse		     = module_dal->get_LinksInUse();
  m_totalROSLinkResets	     = module_dal->get_TotalROSLinkResets();
  m_ignoreROSLinks	     = module_dal->get_IgnoreROSLinks();
  m_reloadFirmware	     = module_dal->get_ReloadFirmware();
  m_reloadSRAMs              = module_dal->get_ReloadSRAMs();
  m_isServerName	     = readOutConfig->get_ISServerName();
  m_dryRun		     = module_dal->get_DryRun();
  if(m_dryRun)
    { FTK_WARNING("ReadoutModule_flic.DryRun option set in DB, IPBus calls with be skipped!" ) };

  // Initialize flic_comm object to open a socket for communicating with board
  // using the IP information included in the segment DB
  // (Readout_flic.schema.xml attributes)
  m_flic = new flic_comm( m_flicip.c_str() , m_port.c_str() , m_hostip.c_str() );

  // Creating the ISInfo object
  std::string isProvider = m_isServerName + "." + m_name;
  ERS_LOG("IS: publishing in " << isProvider);
  try 
  { 
           //FIXME: remove this hack when migration to cmake is complete
    #if __GNUC__ == 4
    m_flicNamed = new ::ftk::FTK_RCD_AtcaCrate_FlicNamed( m_ipcpartition, isProvider.c_str() ); 
    #else
    m_flicNamed = new FTK_RCD_AtcaCrate_FlicNamed( m_ipcpartition, isProvider.c_str() ); 
    #endif 
  }
  catch( ers::Issue &ex)
    {
      daq::ftk::ISException e(ERS_HERE, "Error while creating flicNamed object", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Creating the Histogramming provider
  std::string OHServer("Histogramming"); // TODO: make it configurable
  std::string OHName("FTK_" + m_name);   // Name of the pubblication directory
  try {
    m_ohProvider = new OHRootProvider ( m_ipcpartition , OHServer, OHName );
    ERS_LOG("OH: publishing in " << OHServer << "." << m_name << ", from application " << m_appName);
  }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception.
      daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Construct the histograms
  for( ushort ich = 0 ; ich < 4 ; ich++ ) {

    TH1F *hGlobalStatus = new TH1F( TString::Format("hGlobalStatus.Channel%i",ich) , TString::Format("GlobalStatus.Channel%i;;Status",ich) , 16 , 0 , 16 );
    hGlobalStatus->GetXaxis()->SetBinLabel( 1 , "SFP_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 2 , "CCMUX_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 3 , "SRAM_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 4 , "MERGE_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 5 , "RTM_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 6 , "COLLECTED_PIPELINE_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 7 , "U3_TAGGED_EVENT_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 8 , "U4_TAGGED_EVENT_FIFO_PROG_FULL" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 9 , "SLINK_LDOWN" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 10 , "SLINK_LFF" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 11 , "SLINK_XOFF" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 12 , "NOT_IN_SYNC" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 13 , "SLINK_CTL2_REG1" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 14 , "SLINK_CTL2_REG2" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 15 , "RESERVED" );
    hGlobalStatus->GetXaxis()->SetBinLabel( 16 , "RESERVED" );
    m_hGlobalStatus.push_back( hGlobalStatus );

    TH1F *hPipelineCounters = new TH1F( TString::Format("hPipelineCounters.Channel%i",ich) , TString::Format("PipelineCounters.Channel%i;;Count",ich) , 16 , 0 , 16 );
    hPipelineCounters->GetXaxis()->SetBinLabel( 1 , "SFP_FIFO_FULL_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 2 , "NOT_IN_SYNC_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 3 , "HEADER_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 4 , "TRACK_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 5 , "TRAILER_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 6 , "TRUNCATION_ERROR_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 7 , "CCMUX_FIFO_EVENT_END_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 8 , "SRAM_FIFO_EVENT_READ_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 9 , "MERGE_FIFO_EVENT_READ_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 10 , "U3_TAGGED_EVENT_FIFO_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 11 , "U4_TAGGED_EVENT_FIFO_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 12 , "RTM_FIFO_EVENT_END_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 13 , "FLIC_FLOW_CTL" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 14 , "L1_ID_MATCH_U3_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 15 , "L1_ID_MATCH_U4_COUNT" );
    hPipelineCounters->GetXaxis()->SetBinLabel( 16 , "L1_SKIP_ERRORs" );
    m_hPipelineCounters.push_back( hPipelineCounters );
  }
  m_hTrackSector = new TH1F( "hTrackSector" , "TrackSector;Sector;Monitored Ev" , 32 , 0 , 8192 );
  m_hTrackChi2	 = new TH1F( "hTrackChi2" , "TrackChi2;#chi^{2};Monitored Ev" , 25 , 0 , 500 );
  m_hTrackD0	 = new TH1F( "hTrackD0" , "TrackD0;d_{0} [mm];Monitored Ev" , 30 , -3.0 , 3.0 );
  m_hTrackZ0	 = new TH1F( "hTrackZ0" , "TrackZ0;z_{0} [mm];Monitored Ev" , 40 , -200.0 , 200.0 );
  m_hTrackCotth	 = new TH1F( "hTrackCotth" , "TrackCotth;cot#theta;Monitored Ev" , 30 , -6 , 6 );
  m_hTrackPhi	 = new TH1F( "hTrackPhi" , "TrackPhi;#phi_{0};Monitored Ev" , 32 , -8 , 8 );
  m_hTrackCurv	 = new TH1F( "hTrackCurv" , "TrackCurv;1/2*p_{T} [GeV^{-1}];Monitored Ev" , 25 , -0.5 , 0.5 );

    
  //
  // Now for the actual board setup commands
  //

  // Figure out which FW to put on each FPGA based on user input
  m_ProcTargets.clear();
  m_SSBeTargets.clear();
  if( m_loopbackMode ) {
    m_ProcTargets.push_back( FLIC::targetFpgaU1 );
    m_SSBeTargets.push_back( FLIC::targetFpgaU2 );
  } else if( m_useAsSSBe ) {
    m_SSBeTargets.push_back( FLIC::targetFpgaU1 );
    m_SSBeTargets.push_back( FLIC::targetFpgaU2 );
  } else {
    m_ProcTargets.push_back( FLIC::targetFpgaU1 );
    m_ProcTargets.push_back( FLIC::targetFpgaU2 );
  }

  ERS_LOG( "Processing FPGA targets: " );
  for( ushort target : m_ProcTargets ) ERS_LOG( "    " << target );
  ERS_LOG( "SSB emulator FPGA targets: " );
  for( ushort target : m_SSBeTargets ) ERS_LOG( "    " << target );
  
  ERS_LOG("ReadoutModule_flic::setup: Done");
}


/********************************/
void ReadoutModule_flic::configure(const daq::rc::TransitionCmd& cmd)
/********************************/
{
  ERS_LOG(cmd.toString());

  //
  // The program firmware and initialize links/state machines code below
  // was initially in "setup", but moved here on 2017.1.19 to adhere to the
  // guidelines suggested here:
  //   https://twiki.cern.ch/twiki/bin/view/Atlas/FtkRcTransitions
  //
  
  // Start by reloading the FW if the user requested it
  if( m_reloadFirmware ) {
    ERS_LOG( "Transfering firmware from FLASH to FPGAs" );
    // Ensure that the FPGAs are powered on...
    m_ctrl.PICPowerOn( m_flic );
    // Transfer firmware and lookup tables binaries in Flash to FPGAs and SRAMs
    if( m_ctrl.ProgramFPGAs( m_flic , m_ProcTargets , m_SSBeTargets , m_reloadSRAMs ) < 0 )
      ERS_LOG( "Unable to load firmware for some reason..." );
  }

  // Ensure that the clock rates are all correct.
  // We just check clocks on the processing FPGAs here.
  for( ushort target : m_ProcTargets )
    m_ctrl.CheckClockCounters( m_flic , target );

  // Initialize Tx and some of the state machines
  // Idle board
  /*
  FLICSetupTool t;
  t.setFlicComm( m_flic );
  if( m_loopbackMode ) {
    t.INIT_U1_U2_U3_U4();
  } else if( m_useAsSSBe ) {
    t.INIT_U2_U2_U3_U4();
  } else {
    t.INIT_U1_U1_U3_U4();
  }
  */

  // JW - 2017.2.24 - Testing older config for more reliable flic performance
  // Put SFPs in "normal mode" rather than "setup" mode so we don't confuse the SSB with non-comma words
  //m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0004 , 0x00F0 ); // RW - 2017.6.22 - should move out of the setup mode after event send to SSB for snyc
  // Setup
  m_ctrl.IdleBoard( m_flic );
  m_ctrl.InitStateMachines( m_flic );

  //
  // The stuff below here was the original "configure" setup
  // before the above lines were added on 2017.1.19
  //

  // Whack the Rx's on both U1 and U2
  // This was movied to "connect" 2017.1.20
  //m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  //m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );

  // If OKS specified to ignore ROS links, do that here...
  if( m_ignoreROSLinks ) {
    for( ushort target : m_ProcTargets ) {
      m_ctrl.DumpDataOnTheFloorMode( m_flic , target );
    }
  }

  ERS_LOG(cmd.toString() << " Done");
}

/**************************/
void ReadoutModule_flic::unconfigure(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void ReadoutModule_flic::connect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  // Whack the Rx's again on both U1 and U2 since we're
  // really supposed to do this in "connect"
  // Alignment should be automatically handled in Rx state machine
  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
  m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );

  //  Reset flow control FIFOs now that Tx's and Rx's are initlialized
  // This could in theory be done after just initializing the Tx's. However, when
  // we reset the Rx's the state machine automatically sends a flow control signal
  // to SSB to temporarily pause data, so it's cleaner to reset the flow control
  // FIFOs after the Rx reset is complete.
  for( ushort target : m_ProcTargets )
    m_ctrl.ResetArmFlowControlFIFOs( m_flic , target );
  
  // Try to initialize EMon data handler
  m_ftkemonDataOut = reinterpret_cast<daq::ftk::FtkEMonDataOut*>(ROS::DataOut::sampled());
  if( !m_ftkemonDataOut ) {
    FTK_WARNING("EMonDataOut plugin not found! SpyBuffer data will not be published in emon");
  } else {
    m_ftkemonDataOut->setScalingFactor(1);
    ERS_LOG("Found EMonDataOut plugin");
  }

  // Make sure the clock counters are set in rate mode
  // Commented out for now (2017.1.19) since clock counters should already
  // be in rate mode... note that we have a line in "configure" to
  // check the clock rates.
  //for( ushort target : m_ProcTargets )
  //  m_ctrl.InitClockCounters( m_flic , target , true );

  //
  // Sync FLIC Processing state machines with SSBs or SSB emulator FPGAs
  //
  
  if( m_SSBeTargets.size() > 0 ) {

    // When running with SSB emulator in loopback, the emulator just needs to send a single dummy
    // event to the processor, and then reset the L1ID back to zero

    for( ushort target : m_SSBeTargets ) {
      m_ctrl.StartSSBeData( m_flic , target , 0xf /*channel mask*/ , 1 /*event*/ , 0 /*tracks*/ , 4000 /*commas*/ );
      m_ctrl.ResetSSBe( m_flic , target );
    }
  }

  /*
  // FIXME - Added diagnostic information to help debug SSB-FLIC synch
  //standalone::ReadMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 );
  standalone::DumpGlobalStatus( m_flic , FLIC::targetFpgaU1 );
  standalone::DumpRawSLINKStatus( m_flic , FLIC::targetFpgaU1 );
  standalone::ReadFlowControlFIFOs( m_flic , FLIC::targetFpgaU1 );
  m_ctrl.ResetArmFlowControlFIFOs( m_flic , FLIC::targetFpgaU1 );
  m_ctrl.ResetArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 , 0 );
  */  

  if( m_ProcTargets.size() > 0 ) {

    // When running with the actual SSB, the processor FPGAs need to send a sync control word to each SSB link.
    // The SSBs will then respond by sending back a dummy event. (I don't need to do this exchange when testing the
    // SSBe, since the SSBe sends a dummy event regardless of whether it sees the sync control word.)
    //
    //   The control word was pushed to prepareForRun to fix synchronization timing, but this created more
    // problems because data would start flowing before the boards sync'd. Lets try adding
    // the sync request here, but continuing to request until the FLIC is happy with the sync.

    for( ushort iAttempt = 0 ; true ; iAttempt++ ) {

      // Throw error if we have exceeded the max sync attempts allowed by user
      if( iAttempt == m_totalSyncAttempts ) {
	// throw error
	daq::rc::HardwareError e( ERS_HERE , "FLIC exceeded max sync attempts with SSB" , m_appName.c_str() );
	ers::error( e );
	break;
      }

      if( m_verbose > 0 ) ERS_LOG( "FLIC attempting to sync with SSB [attempt " << (iAttempt+1) << " / " << m_totalSyncAttempts << "]" );
      
      // Send sync word to SSB
      if( !m_loopbackMode ) // Only bother when we are talking to real SSB
	for( ushort target : m_ProcTargets )
	  m_ctrl.SyncWithSSB( m_flic , target );

      // Wait a bit before checking if the sync worked
      usleep( m_delayBetweenSyncAttempts * 1000000 );

      // Now check to see if the FLIC thinks the boards are sync'd
      bool linksHappy = true;
      for( ushort target : m_ProcTargets ) {
	ushort global_status[4];
	m_flic->blockread( target , FLIC::cmdArrayRead , 0x13C , global_status , 4 );
	for( ushort iLink = 0 ; iLink < 4 ; iLink++ ) {
	  ushort globalLink = iLink + (target*4);
	  if( ((m_linksInUse>>globalLink)&1) == 0 ) continue; // We don't care about this link
	  if( ((global_status[iLink]>>11)&1) == 1 ) {
	    linksHappy = false;
	    if( m_verbose > 0 ) {
	      ERS_LOG( "Link " << globalLink << " did NOT sync, GLOBAL_STATUS = " << std::hex << global_status[iLink] << std::dec );
	      standalone::DumpGlobalStatus( m_flic , target );
	      standalone::DumpRawSLINKStatus( m_flic , target );
	    }
	  } else {
	    if( m_verbose > 0 ) ERS_LOG( "Link " << globalLink << " is happy" );
	  }
	  /*
	    // FIXME - Added diagnostics for debugging SSB-FLIC synch
	    standalone::ReadMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 );
	    standalone::DumpGlobalStatus( m_flic , FLIC::targetFpgaU1 );
	    standalone::DumpRawSLINKStatus( m_flic , FLIC::targetFpgaU1 );
	    standalone::ReadFlowControlFIFOs( m_flic , FLIC::targetFpgaU1 );
	    m_ctrl.ResetArmFlowControlFIFOs( m_flic , FLIC::targetFpgaU1 );
	    m_ctrl.ResetArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 , 0 );
	  */
	}
      }
      
      // Stop making sync attempts if all the links are happy
      if( linksHappy ) {
	if( m_verbose > 0 ) ERS_LOG( "All of the specified links have sync'd" );
	break;
      }

      // FIXME - Do a full reconigure of the FLIC before trying to synch again with SSB
      // This was added to help fix synch with SSB at P1, but really should be updated and
      // removed once the problem is understood from the SSB end!!
      if( true ) {
	ERS_LOG( "Doing hard reset of all FLIC state machines. This seems to be the only way to recover comm with SSB at P1" );

	/*
	FLICSetupTool t;
	t.setFlicComm( m_flic );
	if( m_loopbackMode ) {
	  t.INIT_U1_U2_U3_U4();
	} else if( m_useAsSSBe ) {
	  t.INIT_U2_U2_U3_U4();
	} else {
	  t.INIT_U1_U1_U3_U4();
	}
	*/
	
	// JW - 2017.2.24 - Testing older config for more reliable flic performance
	// Setup
	m_ctrl.IdleBoard( m_flic );
	m_ctrl.InitStateMachines( m_flic );
	
	// If OKS specified to ignore ROS links, do that here...
	if( m_ignoreROSLinks ) {
	  for( ushort target : m_ProcTargets ) {
	    m_ctrl.DumpDataOnTheFloorMode( m_flic , target );
	  }
	}
	m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
	m_flic->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      }
      
    } // Done looping over sync attempts
  } // End if ProcTargets > 0


  // Put SFPs in "normal mode" rather than "setup" mode so we don't confuse the SSB with non-comma words

  // Reset and initialize the processor counters
  // Also reset and arm the monitoring FIFOs, spying on channel 0 by default
  for( ushort target : m_ProcTargets ) {
    m_ctrl.InitProcCounters( m_flic , target , 0x0000 ); // 0 => Count mode; 0xffff => Rate mode
    m_ctrl.ResetArmMonitoringFIFOs( m_flic , target , 0 );
  }
  
  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void ReadoutModule_flic::disconnect(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());
  // Fill ME
  ERS_LOG(cmd.toString() << " done");
}



/**********************************/
void ReadoutModule_flic::prepareForRun(const daq::rc::TransitionCmd& cmd)
/**********************************/
{
  ERS_LOG(cmd.toString());

  m_flic->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0004 , 0x00F0 );//RW 2017.6.22, leave setup modle and waiting for data
  // Start sending continuous data on all of the SSB emualators
  for( ushort target : m_SSBeTargets )
    m_ctrl.StartSSBeData( m_flic , target , 0xf /*channel map*/ , m_totalRecords , m_tracksPerRecord , m_delayBetweenRecords );
  
  ERS_LOG(cmd.toString() << " Done");
}


/**************************/
void ReadoutModule_flic::stopDC(const daq::rc::TransitionCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  // Stop sending continuous data on all SSB emulators
  for( ushort target : m_SSBeTargets )
    m_ctrl.StopSSBeData( m_flic , target , 0xf ); // 0xf = stop continuous data on all 4 channels

  // Publish final numbers
  publish();

  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void ReadoutModule_flic::publish()
/**************************/
{
  ERS_LOG("Publishing...");

  // Collect the relevant information from VME
  // TODO: fill

  // Publish in IS with schema, i.e. fill ISInfo object

  // PIC registers
  m_flicNamed->IPMCPowerStat   = m_ctrl.GetRegister( m_flic , FLIC::targetMgmtFpga , 0x0 );
  m_flicNamed->PowerControlMod = m_ctrl.GetRegister( m_flic , FLIC::targetMgmtFpga , 0x2 );

  // Other attributes to be monitored in IS
  // Resize

  // XOFF counters
  m_flicNamed->Pipeline0.resize(4);
  m_flicNamed->SSB_XOFF.resize(4);
  m_flicNamed->SFP_FIFO.resize(4);
  m_flicNamed->CCMUX_FIFO.resize(4);
  m_flicNamed->SRAM_FIFO.resize(4);
  m_flicNamed->MERGE_FIFO.resize(4);
  m_flicNamed->RTM_FIFO.resize(4);

  // GLOBAL status words
  m_flicNamed->GLOBAL_STATUS.resize(4);
  m_flicNamed->RAW_SLINK_STATUS.resize(4);

  // Pipeline counters
  m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT.resize(4);
  m_flicNamed->NOT_IN_SYNC_COUNT.resize(4);
  m_flicNamed->HEADER_ERROR_COUNT.resize(4);
  m_flicNamed->TRACK_ERROR_COUNT.resize(4);
  m_flicNamed->TRAILER_ERROR_COUNT.resize(4);
  m_flicNamed->TRUNCATION_ERROR_COUNT.resize(4);
  m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT.resize(4);
  m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT.resize(4);
  m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT.resize(4);
  m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT.resize(4);
  m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT.resize(4);
  m_flicNamed->RTM_FIFO_EVENT_END_COUNT.resize(4);
  m_flicNamed->FLIC_FLOW_CTL.resize(4);
  m_flicNamed->L1_ID_MATCH_U3_COUNT.resize(4);
  m_flicNamed->L1_ID_MATCH_U4_COUNT.resize(4);
  m_flicNamed->L1_SKIP_ERRORs.resize(4);

  // Monitoring fifos
  m_flicNamed->MonSlice0.resize(10);
  m_flicNamed->MonSlice1.resize(10);
  m_flicNamed->MonSlice2.resize(10);
  m_flicNamed->MonSlice3.resize(10);

  // We need to still decide on this one
  // This is under development
  m_flicNamed->Pipeline0[0] = m_ctrl.GetRegister( m_flic , FLIC::targetFpgaU1 , 0x0105 ); // CCMUX_STATUS_REG

  // Control words
  m_flicNamed->SLINK_CTL_REG2 = m_ctrl.GetRegister( m_flic , FLIC::targetFpgaU1 , 0x9 );
  m_flicNamed->SFP_CTL_REG = m_ctrl.GetRegister( m_flic , FLIC::targetFpgaU2 , 0x4 );
  ushort error_mask[2];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x15 , error_mask , 2 );
  m_flicNamed->FLIC_ERROR_MASK = (error_mask[1]<<16) + error_mask[0];
  
  // XOFFs from various parts of the pipeline
  ushort xoff_counts[28];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x0124 , xoff_counts , 28 );
  for( ushort i = 0 ; i < 4 ; i++ ) {
    // Loop over the four channels in U1
    m_flicNamed->SSB_XOFF[i]   = xoff_counts[0+i]; // SSB_XOFF
    m_flicNamed->SFP_FIFO[i]   = xoff_counts[4+i]; // SFP_FIFO
    m_flicNamed->CCMUX_FIFO[i] = xoff_counts[8+i]; // CCMUX_FIFO
    m_flicNamed->SRAM_FIFO[i]  = xoff_counts[12+i]; // SRAM_FIFO
    m_flicNamed->MERGE_FIFO[i] = xoff_counts[16+i]; // MERGE_FIFO
    m_flicNamed->RTM_FIFO[i]   = xoff_counts[20+i]; // RTM_FIFO

    // The global status word is read from registers 0x13C - 0x13F, immediately after the
    // XOFF counts. We grab it in the same block read to save time.
    m_flicNamed->GLOBAL_STATUS[i] = xoff_counts[24+i]; // GLOBAL STATUS words

    for( ushort flag = 0 ; flag < 16 ; flag++ ) {
      m_hGlobalStatus[i]->SetBinContent( flag+1 , (xoff_counts[24+i]>>flag) & 0x1 );
    }
    
    // Status definitions:
    //   FLIC_GLOBAL_STATUSes(i)(0) <=  SFP_FIFO_PROG_FULLs(i);         --      16: SFP input FIFO Prog Full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(1) <=  CCMUX_FIFO_PROG_FULLs(i);       --      17: CCMUX FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(2) <=  SRAM_FIFO_PROG_FULLs(i);        -- 18: SRAM Lookup FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(3) <=  MERGE_FIFO_PROG_FULLs(i);       -- 19: Merge FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(4) <=  RTM_FIFO_PROG_FULLs(i); -- 20: RTM FIFO Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(5) <=  COLLECTED_PIPELINE_PROG_FULLs(i)(5);    -- 21: User test Prog Full is set (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(6) <=  U3_TAGGED_EVENT_FIFO_PROG_FULLs(i);     -- 22: Spy buffer Tag FIFO #1 Prog full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(7) <=  U4_TAGGED_EVENT_FIFO_PROG_FULLs(i);     -- 23: Spy buffer Tag FIFO #2 Prog full is set  (source of flow control to SSB)
    //   FLIC_GLOBAL_STATUSes(i)(8) <=  SLINK_LDOWN_FLAGs(i);   -- 24: SLINK went down since last event was transmitted
    //   FLIC_GLOBAL_STATUSes(i)(9) <=  SLINK_LFF_FLAGs(i);     -- 25: SLINK FIFO FULL  (LFF)
    //   FLIC_GLOBAL_STATUSes(i)(10) <=  SLINK_XOFF_FLAGs(i);   -- 26: SLINK XOFF
    //   FLIC_GLOBAL_STATUSes(i)(11) <=  NOT_IN_SYNCs(i);       -- 27: NOT_IN_SYNC (Core Crate Receiver not synchronized to input)
    //   FLIC_GLOBAL_STATUSes(i)(12) <=  SLINK_CTL2_REG(i+4);   -- 28: User Defined status bit 1
    //   FLIC_GLOBAL_STATUSes(i)(13) <=  SLINK_CTL2_REG(i+8);   -- 29: User Defined status bit 2
    //   FLIC_GLOBAL_STATUSes(i)(14) <=  '0';   -- 30: Reserved for use as enable/disable of checking the STAT FIFOS
    //   FLIC_GLOBAL_STATUSes(i)(15) <=  '0';   -- 31: Reserved

  }

  // In addition to the global status, we can get the raw status of the SLINKs
  ushort slink_status;
  m_flic->singleread( FLIC::targetFpgaU1 , FLIC::cmdRead , 0x10E , &slink_status );
  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_flicNamed->RAW_SLINK_STATUS[i] = (slink_status>>(i*4))&0xf;
  }
    
  // Clock frequencies
  ushort clock_counts_u[2][8];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x0106 , clock_counts_u[0] , 8 );
  //m_flic->blockread( FLIC::targetFpgaU2 , FLIC::cmdArrayRead , 0x0106 , clock_counts_u[1] , 8 );
  for( ushort i = 0 ; i < 2 ; i++ ) {
    // Loop over U1 and U2
    // (The U2 counts are not read for the moment since the SSBe firmware is being loaded)
    m_flicNamed->SRAM_REFCLK[i]   = clock_counts_u[i][0];
    m_flicNamed->DDR_REFCLK[i]    = clock_counts_u[i][1];
    m_flicNamed->GTX112_REFCLK[i] = clock_counts_u[i][2];
    m_flicNamed->GTX112_TXCLK[i]  = clock_counts_u[i][3];
    m_flicNamed->GTX112_RXCLK[i]  = clock_counts_u[i][4];
    m_flicNamed->GTX116_REFCLK[i] = clock_counts_u[i][5];
    m_flicNamed->GTX116_TXCLK[i]  = clock_counts_u[i][6];
    m_flicNamed->GTX116_RXCLK[i]  = clock_counts_u[i][7];
  }


  // Processor counters
  for( ushort i = 0 ; i < 4 ; i++ ) {
    ushort proc_counts[0x10];
    m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x310+(i*0x10) , proc_counts , 0x10 );
    m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT[i]   = proc_counts[0x0];
    m_flicNamed->NOT_IN_SYNC_COUNT[i]           = proc_counts[0x1];
    m_flicNamed->HEADER_ERROR_COUNT[i]          = proc_counts[0x2];
    m_flicNamed->TRACK_ERROR_COUNT[i]           = proc_counts[0x3];
    m_flicNamed->TRAILER_ERROR_COUNT[i]         = proc_counts[0x4];
    m_flicNamed->TRUNCATION_ERROR_COUNT[i]      = proc_counts[0x5];
    m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[i]  = proc_counts[0x6];
    m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT[i]  = proc_counts[0x7];
    m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT[i] = proc_counts[0x8];
    m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT[i]  = proc_counts[0x9];
    m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT[i]  = proc_counts[0xa];
    m_flicNamed->RTM_FIFO_EVENT_END_COUNT[i]    = proc_counts[0xb];
    m_flicNamed->FLIC_FLOW_CTL[i]               = proc_counts[0xc];
    m_flicNamed->L1_ID_MATCH_U3_COUNT[i]        = proc_counts[0xd];
    m_flicNamed->L1_ID_MATCH_U4_COUNT[i]        = proc_counts[0xe];
    m_flicNamed->L1_SKIP_ERRORs[i]              = proc_counts[0xf];

    // Fill counter histogram
    for( ushort icounter = 0 ; icounter < 16 ; icounter++ ) {
      m_hPipelineCounters[i]->Fill( icounter , proc_counts[icounter] - m_last_proc_counts[i][icounter] );
      m_last_proc_counts[i][icounter] = proc_counts[icounter];
    }
    
  }
  m_flicNamed->CCMUX_EVENTS_CHANNEL0 = m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[0];

  // Get the most recent L1IDs
  ushort l1ids[2];
  m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdArrayRead , 0x14E , l1ids , 2 );
  m_flicNamed->CURRENT_L1ID = l1ids[1] + (l1ids[0]<<16);

  // Dump new words that show up in channel 0 flow control monitoring fifo
  if( m_verbose > 0 ) {
    // First read the FIFO
    ushort data[256];
    m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x118 , data , 256 );
    //for( size_t iword = 0 ; iword < 100 ; iword++ ) {
      // temporary dump of FIFO for added debugging
      //ERS_LOG( "Data flow word " << iword << " = " << std::hex << data[iword] << std::dec );
    //}
    for( size_t iword = 0 ; iword < 255 ; iword += 3 ) {
      if( data[iword] == data[iword+1] && data[iword]==data[iword+2] ) break;
      // otherwise we have found a new flow control word and time stamp
      uint word = data[iword];
      uint time = data[iword+2] + (data[iword+1]<<16);
      ERS_LOG( "FLIC sent flow control word " << std::hex << word << " at timestamp=" << std::hex << time << std::dec );
    }
    // Reset the FIFO
    m_ctrl.ResetArmFlowControlFIFOs( m_flic , FLIC::targetFpgaU1 );
  }

  // Dump some monitoring FIFO data to the log file
  if( m_verbose > 1 ) {
    
    ushort data[4][256];
    for( ushort i = 0 ; i < 4 ; i++ )
      m_flic->blockread( FLIC::targetFpgaU1 , FLIC::cmdFifoRead , 0x110+(2*i) , data[i] , 256 );

    if( data[0][0]==0x0 && data[0][1]==0x0 && data[0][2]==0x0 && data[0][3]==0x0 && data[0][4]==0x0 ) {
      // Looks like FIFOs are empty so don't bother printing
      // a bunch of zeros
    } else  {
      std::cout << "Monitoring FIFO Data for FPGA " << FLIC::targetFpgaU1 << std::endl;
      std::cout << std::setw(10) << "iAddr" << " : " << std::setw(10) << "M0" << std::setw(10) << "M1" << std::setw(10) << "M2" << std::setw(10) << "M3" << std::endl;
      std::cout << std::hex;
      for( ushort iword = 0 ; iword < 256 ; iword++ )
	std::cout << std::setw(10) << iword << " : "
		  << std::setw(10) << data[0][iword]
		  << std::setw(10) << data[1][iword]
		  << std::setw(10) << data[2][iword]
		  << std::setw(10) << data[3][iword]
		  << std::endl;
      std::cout << std::dec;
    }
    
    // Re-arm the monitoring FIFOs so they will catch to next available event, which
    // will get printed on the next call to publish
    m_ctrl.ArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 );
    
  }
    

  
  // Monitoring FIFOs
  if( false ) { // turning off monitoring fifo reads for the moment -JW

    vector< pair< shared_ptr<SpyBuffer> , uint32_t > > vec_MonFifos;
    for( ushort imon = 0 ; imon < 4 /*Number of FIFOs in a single pipeline*/ ; imon++ ) {

      shared_ptr<FlicMonitoringFIFO> monFifo = make_shared< FlicMonitoringFIFO>( FLIC::targetFpgaU1 , imon , 256 );
      monFifo->read( m_flic );
      uint32_t sourceId = encode_SourceIDSpyBuffer( BoardType::Flic , m_boardNumber , imon , Position::OTHER_POSITION );
      vec_MonFifos.push_back( make_pair(monFifo,sourceId) );

      // Put the first 10 words from each fifo in IS
      for( ushort iword = 0 ; iword < 10 ; iword++ ) {
        if( imon==0 )      m_flicNamed->MonSlice0[iword] = monFifo->getBuffer()[iword];
        else if( imon==1 ) m_flicNamed->MonSlice1[iword] = monFifo->getBuffer()[iword];
        else if( imon==2 ) m_flicNamed->MonSlice2[iword] = monFifo->getBuffer()[iword];
        else if( imon==3 ) m_flicNamed->MonSlice3[iword] = monFifo->getBuffer()[iword];
      }

      // Scan MonFifo0 and extract some track information
      if( imon==0 ) {
	for( ushort iword = 0 ; iword < 256 - 12 ; iword++ ) {
	  if( ((monFifo->getBuffer()[iword]) & 0xfff) == 0xbda ) {
	    m_hTrackSector->Fill( monFifo->getBuffer()[iword+1] );
	    m_hTrackChi2->Fill( monFifo->getBuffer()[iword+6] );
	    m_hTrackD0->Fill( monFifo->getBuffer()[iword+7] );
	    m_hTrackZ0->Fill( monFifo->getBuffer()[iword+8] );
	    m_hTrackCotth->Fill( monFifo->getBuffer()[iword+9] );
	    m_hTrackPhi->Fill( monFifo->getBuffer()[iword+10] );
	    m_hTrackCurv->Fill( monFifo->getBuffer()[iword+11] );
	  }
	}
      }
      
      // Scan MonFifo0 for an example RunNumber and Lvl1ID to put in IS
      /*
      if( imon==0 ) {
        for( ushort iword = 0 ; iword < 256 - 7 ; iword++ ) {
          if( monFifo->getBuffer()[iword] != 0xB0F0 ) continue;
          if( monFifo->getBuffer()[iword+1] != 0xCAF0 ) continue;
          if( monFifo->getBuffer()[iword+2] != 0xFF12 ) continue;
          if( monFifo->getBuffer()[iword+3] != 0xFF34 ) continue;
          break;
        }
      }
      */

      
    }

    // Arm MonFifos for next call to publish
    m_ctrl.ArmMonitoringFIFOs( m_flic , FLIC::targetFpgaU1 );

    // Ship Mon FIFO data to EMon
    if( m_ftkemonDataOut ) {
      ERS_LOG( "Shipping Monitoring FIFO data to EMonDataOut ..." );
      m_ftkemonDataOut->sendData( vec_MonFifos );
      ERS_LOG( "Shipment complete" );
    }

  }

  // Check in the IS info
  try { m_flicNamed->checkin(); }
  catch ( daq::oh::Exception & ex)
    { // Raise a warning or throw an exception.
      daq::ftk::ISException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
      ers::warning(e);  //or throw e;
    }

  // Fill and publish histograms
  // Example:
  //m_histogram->Fill( rand()%100 - 50 );
  try {
    //m_ohProvider->publish( *m_hGlobalStatus , m_hGlobalStatus->GetName() , true );
    for( ushort i = 0 ; i < 4 ; i++ ) {
      m_ohProvider->publish( *m_hGlobalStatus[i] , m_hGlobalStatus[i]->GetName() , true );
      m_ohProvider->publish( *m_hPipelineCounters[i] , m_hPipelineCounters[i]->GetName() , true );
    }
    m_ohProvider->publish( *m_hTrackSector , m_hTrackSector->GetName() , true );
    m_ohProvider->publish( *m_hTrackChi2 , m_hTrackChi2->GetName() , true );
    m_ohProvider->publish( *m_hTrackD0 , m_hTrackD0->GetName() , true );
    m_ohProvider->publish( *m_hTrackZ0 , m_hTrackZ0->GetName() , true );
    m_ohProvider->publish( *m_hTrackCotth , m_hTrackCotth->GetName() , true );
    m_ohProvider->publish( *m_hTrackPhi , m_hTrackPhi->GetName() , true );
    m_ohProvider->publish( *m_hTrackCurv , m_hTrackCurv->GetName() , true );
  }
  catch( daq::oh::Exception & ex) { // Raise a warning or throw an exception.
    daq::ftk::OHException e(ERS_HERE, "", ex);  // NB: append the original exception (ex) to the new one
    ers::warning(e);  //or throw e;
  }

  //
  // Before ending publish, do some "stopless removal" checks...
  // FIXME!
  //
  // Check that ROS links are up, i.e. that LDOWN = 0 on channel 0
  /*
  if( (m_flicNamed->GLOBAL_STATUS[0] >> 8) & 1 ) {
    daq::rc::HardwareError e( ERS_HERE , (m_name+".Channel0").c_str() , m_appName.c_str() );
    ers::error( e );
  }
  */
  
  /*
  if( m_flicNamed->CCMUX_EVENTS_CHANNEL0 > 10000 ) {
    daq::rc::HardwareError e( ERS_HERE , (m_name+".DummyChannel").c_str() , m_appName.c_str() );
    ers::error( e );
  }
  */

}

/**************************/
void ReadoutModule_flic::resynch(const daq::rc::ResynchCmd& cmd)
/**************************/
{
  ERS_LOG(cmd.toString());

  // Try to resynch each Proc with SSBs
  for( ushort target : m_ProcTargets )
    m_ctrl.SyncWithSSB( m_flic , target );

  ERS_LOG(cmd.toString() << " done");
}


/**************************/
void ReadoutModule_flic::clearInfo()
/**************************/
{
  ERS_LOG("Clear histograms and counters");

  // Reset histograms

  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_hGlobalStatus[i]->Reset();
    m_hPipelineCounters[i]->Reset();
  }
  m_hTrackSector->Reset();
  m_hTrackChi2->Reset();
  m_hTrackD0->Reset();
  m_hTrackZ0->Reset();
  m_hTrackCotth->Reset();
  m_hTrackPhi->Reset();
  m_hTrackCurv->Reset();
    
  // Reset the IS counters

  m_flicNamed->IPMCPowerStat   = 0 ;
  m_flicNamed->PowerControlMod = 0 ;

  m_flicNamed->Pipeline0[0] = 0 ;

  m_flicNamed->SLINK_CTL_REG2  = 0 ;
  m_flicNamed->SFP_CTL_REG     = 0 ;
  m_flicNamed->FLIC_ERROR_MASK = 0 ;

  for( ushort i = 0 ; i < 4 ; i++ ) {
    // Loop over the four pipelines in U1
    m_flicNamed->SSB_XOFF[i]	     = 0 ;
    m_flicNamed->SFP_FIFO[i]	     = 0 ;
    m_flicNamed->CCMUX_FIFO[i]	     = 0 ;
    m_flicNamed->SRAM_FIFO[i]	     = 0 ;
    m_flicNamed->MERGE_FIFO[i]	     = 0 ;
    m_flicNamed->RTM_FIFO[i]	     = 0 ;
    m_flicNamed->GLOBAL_STATUS[i]    = 0 ;
    m_flicNamed->RAW_SLINK_STATUS[i] = 0 ;
  }

  for( ushort i = 0 ; i < 2 ; i++ ) {
    // Loop over U1 and U2
    m_flicNamed->SRAM_REFCLK[i]   = 0 ;
    m_flicNamed->DDR_REFCLK[i]    = 0 ;
    m_flicNamed->GTX112_REFCLK[i] = 0 ;
    m_flicNamed->GTX112_TXCLK[i]  = 0 ;
    m_flicNamed->GTX112_RXCLK[i]  = 0 ;
    m_flicNamed->GTX116_REFCLK[i] = 0 ;
    m_flicNamed->GTX116_TXCLK[i]  = 0 ;
    m_flicNamed->GTX116_RXCLK[i]  = 0 ;
  }

  for( ushort i = 0 ; i < 4 ; i++ ) {
    m_flicNamed->SFP_FIFO_FULL_ERROR_COUNT[i]   = 0 ;
    m_flicNamed->NOT_IN_SYNC_COUNT[i]           = 0 ;
    m_flicNamed->HEADER_ERROR_COUNT[i]          = 0 ;
    m_flicNamed->TRACK_ERROR_COUNT[i]           = 0 ;
    m_flicNamed->TRAILER_ERROR_COUNT[i]         = 0 ;
    m_flicNamed->TRUNCATION_ERROR_COUNT[i]      = 0 ;
    m_flicNamed->CCMUX_FIFO_EVENT_END_COUNT[i]  = 0 ;
    m_flicNamed->SRAM_FIFO_EVENT_READ_COUNT[i]  = 0 ;
    m_flicNamed->MERGE_FIFO_EVENT_READ_COUNT[i] = 0 ;
    m_flicNamed->U3_TAGGED_EVENT_FIFO_COUNT[i]  = 0 ;
    m_flicNamed->U4_TAGGED_EVENT_FIFO_COUNT[i]  = 0 ;
    m_flicNamed->RTM_FIFO_EVENT_END_COUNT[i]    = 0 ;
    m_flicNamed->FLIC_FLOW_CTL[i]               = 0 ;
    m_flicNamed->L1_ID_MATCH_U3_COUNT[i]        = 0 ;
    m_flicNamed->L1_ID_MATCH_U4_COUNT[i]        = 0 ;
    m_flicNamed->L1_SKIP_ERRORs[i]              = 0 ;
    // last counter currently unused
  }
  m_flicNamed->CCMUX_EVENTS_CHANNEL0 = 0 ;
  m_flicNamed->CURRENT_L1ID = 0 ;

  for( ushort i = 0 ; i < 10 ; i++ ) {
    m_flicNamed->MonSlice0[i] = 0 ;
    m_flicNamed->MonSlice1[i] = 0 ;
    m_flicNamed->MonSlice2[i] = 0 ;
    m_flicNamed->MonSlice3[i] = 0 ;
  }

}


//FOR THE PLUGIN FACTORY
extern "C"
{
  extern ROS::ReadoutModule* createReadoutModule_flic();
}

ROS::ReadoutModule* createReadoutModule_flic()
{
  return (new ReadoutModule_flic());
}
