#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"
#include "flic/FLICSetupTool.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flic_ssbe , flic_comm *flic_proc , FLICController ctrl );
int Setup( flic_comm *flic_ssbe , flic_comm *flic_proc );

int main() {
  int bytes = 0;
  FLICController ctrl;

  while( true ) {

    cout << endl;
    cout << "--- SSBe Board ---" << endl;
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic_ssbe = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );

    cout << endl;
    cout << "--- Processor Board ---" << endl;
    b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic_proc = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );

    bytes += Option( flic_ssbe , flic_proc , ctrl );

    delete flic_ssbe;
    delete flic_proc;
  }

  return bytes;
}


int Option( flic_comm *flic_ssbe , flic_comm *flic_proc , FLICController ctrl ) {
  int option = 1;
  int bytes  = 0;
  while( option != 0 ) {
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On ALL" << endl
         << " 100 Power Off ALL" << endl
      //<< " 101 Power On SSBe   102 Power Off SSBe" << endl
      //<< " 103 Power On Proc   104 Power Off Proc" << endl
         << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
         << " Basic initialization controls..." << endl
         << "   2 Program All FPGAs" << endl
         << "   3 Setup SSBe board" << endl
	 << "   4 Setup Proc board" << endl
         << "   5 Send sync event from SSBe board to Proc board" << endl
         << "   6 Reset SSBe" << endl
	 << "   7 Setup Spy buffers for all events on all channels" << endl
	 << standalone::line << endl
	 << "   8 Read register on SSBe board   9 Write register on SSBe board" << endl
	 << "  10 Read register on Proc board  11 Write register on Proc board" << endl
	 << standalone::line << endl
	 << "  76 Send single event" << endl
	 << "  761 Send single event in a while loop" << endl
	 << "  77 Start SSBe data" << endl
         << "  78 Stop SSBe data" << endl
         << standalone::line << endl
         << " Diagnostics..." << endl
         << "  84 Reset and arm monitoring FIFOs" << endl
         << "  85 Dump monitoring FIFOs on U1" << endl
         << "  86 Dump monitoring FIFOs on U2" << endl
	 << "  87 Reset and arm flow control FIFOs" << endl
	 << "  88 Dump flow control FIFOs on U1" << endl
	 << "  89 Dump flow control FIFOs on U2" << endl
         << "  90 Initialize counters" << endl
         << "  91 Dump counters" << endl
	 << "  92 Dump global status" << endl
	 << "  93 Dump raw SLINK status" << endl
         << " 222 Check program status" << endl
         << " 223 Check clock counters" << endl
         << " 224 Check XOFF counters" << endl
         << " 225 Change to \"dump data on the floor\" mode" << endl
	 << " 226 Dump spybuffer IP configuration" << endl
	 << " 227 Dump U3/U4 diagnostics" << endl
         << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) {
      bytes += ctrl.PICPowerOn( flic_ssbe );
      bytes += ctrl.PICPowerOn( flic_proc );
    }
    if (option == 100) {
      bytes += ctrl.PICPowerOff( flic_ssbe );
      bytes += ctrl.PICPowerOff( flic_proc );
    }
    if (option == 101 ) bytes += ctrl.PICPowerOn( flic_ssbe );
    if (option == 102 ) bytes += ctrl.PICPowerOff( flic_ssbe );
    if (option == 103 ) bytes += ctrl.PICPowerOn( flic_proc );
    if (option == 104 ) bytes += ctrl.PICPowerOff( flic_proc );
    if (option == 200) {
      bytes+=ctrl.PICPowerFromIPMC( flic_ssbe );
      bytes+=ctrl.PICPowerFromIPMC( flic_proc );
    }
    if (option == 2) {
      // Program proc board
      ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::DoubleProc_NoSRAMs );
      //ctrl.ProgramFPGAs( flic_proc , FLIC::ListProc::Loopback_Reverse_NoSRAMs ); // FIXME - update name of variable
      // Kick GTX112 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x200 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x200 , 0xffff );
      // Kick GTX116 to make sure clocks come up correctly
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x208 , 0xffff );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x208 , 0xffff );
      // Then check the clocks
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) );
      // Program ssbe board
      ctrl.ProgramFPGAs( flic_ssbe , FLIC::ListProc::DoubleSSBe );
    }
    if (option == 3) {
      FLICSetupTool t;
      t.setFlicComm( flic_ssbe );
      t.INIT_U2_U2_U3_U4();
    }
    if (option == 4) {
      FLICSetupTool t;
      t.setFlicComm( flic_proc );
      t.INIT_U1_U1_U3_U4();
      // Whack the Rx's on both processor and SSBe boards
      /*
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x8888 );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0200 , 0x4444 );
      */
      flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0201 , 0x00FF );
      flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0201 , 0x00FF );
    }
    if (option == 5) {
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
      bytes += ctrl.SyncWithSSB( flic_proc , FLIC::targetFpgaU2 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    }
    if (option == 6) {
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetSSBe( flic_ssbe , FLIC::targetFpgaU2 );
    }
    if (option == 7) {
      FLICSetupTool t;
      t.setFlicComm( flic_proc );
      t.INIT_SPYBUFFERS();
    }
    if (option == 8) standalone::ReadRegister( flic_ssbe );
    if (option == 9) standalone::WriteRegister( flic_ssbe );
    if (option == 10) standalone::ReadRegister( flic_proc );
    if (option == 11) standalone::WriteRegister( flic_proc );
    if (option == 76) {
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 1 , 3 , 100 );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 100 );
    }
    if (option == 761) {
      ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0x1 , 10 , 0 , 4000 , false );
      int nInput =0;      
      printf("input how many times do you want to loop: \n");
      scanf("%d", &nInput);

      for(int i=0; i<nInput; ++i) {
	ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0x1 );
	// usleep( 1000000 );



	//      bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x201 , 1<<12 );

	// bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0x1 , 1 , 3 , 100 );
	//  bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 1 , 3 , 100 );
      }
    }
    if (option == 77) {
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.StartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.RestartSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 78) {
      bytes += ctrl.StopSSBeData( flic_ssbe , FLIC::targetFpgaU1 , 0xf );
      bytes += ctrl.StopSSBeData( flic_ssbe , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 84) {
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 , 0 );
      bytes += ctrl.ResetArmMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 , 0 );
    }
    if (option == 85) bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU1 );
    if (option == 86) bytes += standalone::ReadMonitoringFIFOs( flic_proc , FLIC::targetFpgaU2 );
    if (option == 87) {
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.ResetArmFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 88) bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU1 );
    if (option == 89) bytes += standalone::ReadFlowControlFIFOs( flic_proc , FLIC::targetFpgaU2 );
    if (option == 90) {
      bytes += ctrl.InitProcCounters( flic_proc , FLIC::targetFpgaU1 , 0x0 );
      bytes += ctrl.InitProcCounters( flic_proc , FLIC::targetFpgaU2 , 0x0 );
    }
    if (option == 91) {
      bytes += standalone::DumpProcCounters( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpProcCounters( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 92) {
      bytes += standalone::DumpGlobalStatus( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpGlobalStatus( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 93) {
      bytes += standalone::DumpRawSLINKStatus( flic_proc , FLIC::targetFpgaU1 );
      bytes += standalone::DumpRawSLINKStatus( flic_proc , FLIC::targetFpgaU2 );
    }
    if (option == 222) {
      cout << "--- SSBe Board ---" << endl
           << ctrl.CheckStatus( flic_ssbe ) << endl
           << "--- Processor Board ---" << endl
           << ctrl.CheckStatus( flic_proc ) << endl;
    }
    if (option == 223) {
      cout << ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckClockCounters( flic_proc , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 224) {
      cout << ctrl.CheckXOFFCounters( flic_proc , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckXOFFCounters( flic_proc , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 225) {
      bytes += ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU1 );
      bytes += ctrl.DumpDataOnTheFloorMode( flic_proc , FLIC::targetFpgaU2 );
    }
    if( option == 226 ) bytes += standalone::DumpSpybufferIPConfig( flic_proc );
    if( option == 227 ) bytes += standalone::DumpU3U4Diagnostics( flic_proc );
    //option=0;//One time!
  }//while option
  return bytes;
}


// PW - Feb. 20, 2017: I wonder whether this Setup() function is used in setting up FPGA. I checked the code above and also other code in the "trunk" directory. My inference is that this function is not used anywhere.

int Setup( flic_comm *flic_ssbe , flic_comm *flic_proc ) {

  int bytes = 0;

  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x0060 ); // U2 Set 0x2 0x60 #Put U2 112 SM in Reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x0060 ); // U2 Set 0x2 0x60 #Put U2 112 SM in Reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x6060 ); // U1 Set 0x2 0x6060 #Put U1 112/116 SM in Reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x6060 ); // U1 Set 0x2 0x6060 #Put U1 112/116 SM in Reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0xFFFF ); // U2 Write 0x200 0xFFFF #Whack U2 GTX112
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0xFFFF ); // U2 Write 0x200 0xFFFF #Whack U2 GTX112
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001F , 0x00F0 ); // U2 Write 0x1F 0x0F0 #Set U2 GTX112 Send Commas
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001F , 0x00F0 ); // U2 Write 0x1F 0x0F0 #Set U2 GTX112 Send Commas
  //20150805 : would be prudent here to ensure U2 TDIS is OFF , to ensure that the photons are going out...

  // MBO 20150730: New Code
  // Ok , at this point , we need to look at configuring the internal quads. This is an intial stab and the ideal placement in the setup may be elsewhere.
  //
  // #if 0

  /** suspected bugs in this section **/

  /** No bugs found... **/

  // Put U1-GTX113 and U1-GTX114 and U1-GTX115 SM into reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x078C );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x078C );
  // Put U2-GTX113 and U2-GTX114 and U2-GTX115 SM into reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x078C );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x078C );
  // Put U3-GTX112 and U3-GTX113 and U3-GTX114 SM into reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdSet , 0x00000002 , 0x01EC );
  // Put U4-GTX112 and U4-GTX113 and U4-GTX114 SM into reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdSet , 0x00000002 , 0x01EC );
  // Set U1-GTX113 and U1-GTX114 and U1-GTX115 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000012 , 0x000F ); // GTX113 -- U1<->U2
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000013 , 0x000F ); // GTX114 -- U1<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000014 , 0x000F ); // GTX115 -- U1<->U4
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000012 , 0x000F ); // GTX113 -- U1<->U2
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000013 , 0x000F ); // GTX114 -- U1<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000014 , 0x000F ); // GTX115 -- U1<->U4
  // Set U2-GTX113 and U2-GTX114 and U2-GTX115 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000024 , 0x000F ); // GTX113 -- U2<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000025 , 0x000F ); // GTX114 -- U2<->U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000026 , 0x000F ); // GTX115 -- U2<->U4
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000024 , 0x000F ); // GTX113 -- U2<->U3
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000025 , 0x000F ); // GTX114 -- U2<->U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000026 , 0x000F ); // GTX115 -- U2<->U4
  // Set U3-GTX112 and U3-GTX113 and U3-GTX114 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U3<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U3<->U
  // Set U4-GTX112 and U4-GTX113 and U4-GTX114 to send real data.( commas while idle )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U4<->U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000004 , 0x000F ); // GTX113 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000005 , 0x000F ); // GTX114 -- U4<->U
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000006 , 0x000F ); // GTX115 -- U4<->U1
  // Reset U1-GTX113 and U1-GTX114 and U1-GTX115 RX & TX Gracefully
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0202 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0204 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0206 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0202 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0204 , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0206 , 0xFFFF );
  // Reset U2-GTX113 and U2-GTX114 and U2-GTX115 RX Gracefully
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000206 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  //
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000206 , 0x8888 );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000206 , 0x4444 );
  // Reset U3-GTX112 and U3-GTX113 and U3-GTX114 RX Gracefully
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000200 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  // Reset U4-GTX112 and U4-GTX113 and U4-GTX114 RX Gracefully
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000200 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000200 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000202 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000202 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000204 , 0x8888 );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdWrite , 0x00000204 , 0x4444 );
  // Take U1-GTX113 and U1-GTX114 and U1-GTX115 SM out of reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x078C );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x078C );
  // Take U2-GTX113 and U2-GTX114 and U2-GTX115 SM out of reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x078C );
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x078C );
  // Take U3-GTX112 and U3-GTX113 and U3-GTX114 SM out of reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU3 , FLIC::cmdClear , 0x00000002 , 0x01EC );
  // Take U4-GTX112 and U4-GTX113 and U4-GTX114 SM out of reset
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdClear , 0x00000002 , 0x01EC );


  // DEBUG USE ONLY:
  // Set match registers in U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002A , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002B , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002C , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000002D , 0xFFFF );
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002A , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002B , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002C , 0xFFFF );
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000002D , 0xFFFF );

  // MBO 20150730: End new code , resume Jeremy's prcedure.

  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x8888 ); // U1 Write 0x200 0x8888 #Reset U1 GTX112 RX logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x8888 ); // U1 Write 0x200 0x8888 #Reset U1 GTX112 RX logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x4444 ); // U1 Write 0x200 0x4444 #Reset U1 GTX112 RX PLL

  //JTA: now that we have flow control you have to reset the U1 TX , too( 20150805 )
  // Failure to do this means U1 GTX112 TX clock is not present , means SSB( U2 ) can't
  // get any flow control messages or anything else.
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x2222 ); // U1 Write 0x200 0x2222 #Reset U1 GTX112 TX Logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x2222 ); // U1 Write 0x200 0x2222 #Reset U1 GTX112 TX Logic
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000200 , 0x1111 ); // U1 Write 0x200 0x1111 #Reset U1 GTX112 TX PLL
  // end add

  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000003 , 0x00F0 ); // U1 Set 0x3 0x0F0 #Set RTM TDIS Must be done before GTX116 Resets
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000208 , 0xFFFF ); // U1 Write 0x208 0xFFFF #Whack U1 GTX116
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x6060 ); // U1 Clear 0x2 0x6060 #Take U1 112/116 SM out of Reset
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000003 , 0x00F0 ); // U1 Set 0x3 0x0F0 #Set RTM TDIS Must be done before GTX116 Resets
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000208 , 0xFFFF ); // U1 Write 0x208 0xFFFF #Whack U1 GTX116
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x6060 ); // U1 Clear 0x2 0x6060 #Take U1 112/116 SM out of Reset

  // JTA 20150805 you also have to turn on the SFP transmitters at the front of the board( U1 )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000004 , 0x000F ); //clear TDIS bits in SFP Ctl Reg
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000004 , 0x00F0 ); //Set TX Mux bits of U1 to send flow control data , not PRBS data
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000004 , 0x000F ); //clear TDIS bits in SFP Ctl Reg
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000004 , 0x00F0 ); //Set TX Mux bits of U1 to send flow control data , not PRBS data
  // end add



  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000002 , 0x0060 ); // U2 Clear 0x2 0x60 #Take U2 112 SM out of Reset
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000002 , 0x0060 ); // U2 Clear 0x2 0x60 #Take U2 112 SM out of Reset
  // //
  // // 4. Setup for processing and emulation.


  // should be a Send Set , not a Send Write( JTA 20150805 )
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x0012 ); // U1 Write 0x2 0x12 #Set SRAM access , and enable Core Crate receiver and data merge machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x0012 ); // U1 Write 0x2 0x12 #Set SRAM access , and enable Core Crate receiver and data merge machines
  //end change


  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001F , 0xFFFF ); // U1 Write 0x1F 0xFFFF #Hold All
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001F , 0x0000 ); // U1 Write 0x1F 0x0000 #Clear all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001F , 0xFFFF ); // U1 Write 0x1F 0xFFFF #Hold All
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001F , 0x0000 ); // U1 Write 0x1F 0x0000 #Clear all
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xFFFF ); // U1 Write 0x20F 0xFFFF #Pulse reset all
  // //( One can never be too careful. )
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000201 , 0x000F ); // U2 Write 0x201 0xF #Reset SSB Emulator
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000201 , 0x000F ); // U2 Write 0x201 0xF #Reset SSB Emulator
  //
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdClear , 0x00000003 , 0x0F00 ); // U1 Clear 0x3 0xF00 #Clear GO bit of reset machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000209 , 0x0F00 ); // U1 Write 0x209 0xF00 #Request reset machines to re-arm
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdClear , 0x00000003 , 0x0F00 ); // U1 Clear 0x3 0xF00 #Clear GO bit of reset machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000209 , 0x0F00 ); // U1 Write 0x209 0xF00 #Request reset machines to re-arm
  //? SendSet( targetFpgaU1 , 0x00000002 , 0x0F00 ); // U1 Set 0x2 0xF00 #Set the GO bit to the reset machines
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000002 , 0x0008 ); // U1 Set 0x2 0x8 #Switch SRAM control back to SM
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000002 , 0x0008 ); // U1 Set 0x2 0x8 #Switch SRAM control back to SM
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001D , 0xDDDD ); // U2 Write 0x1D 0xDDDD #SSB emulator status words
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001D , 0xDDDD ); // U2 Write 0x1D 0xDDDD #SSB emulator status words
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000001E , 0xEEEE ); // U2 Write 0x1E 0xEEEE #SSB emulator status words
  bytes += flic_ssbe->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000001E , 0xEEEE ); // U2 Write 0x1E 0xEEEE #SSB emulator status words

  //20160801: FLIC status "word" registers are now the FLIC ERROR MASK registers , should be initialized but to some as-yet unknown value.
  //until that value is known , make 'em 0.
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000015 , 0x0 ); // U1 Write 0x15 0x1515 #FLIC status words
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x00000016 , 0x0 ); // U1 Write 0x16 0x1616 #FLIC status words
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000015 , 0x0 ); // U1 Write 0x15 0x1515 #FLIC status words
  //bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x00000016 , 0x0 ); // U1 Write 0x16 0x1616 #FLIC status words
  //end change 20160801
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdWrite , 0x0000020F , 0xD0F0 ); // U1 Write 0x20F 0xD0F0 #Reset all FIFO flags
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdWrite , 0x0000020F , 0xD0F0 ); // U1 Write 0x20F 0xD0F0 #Reset all FIFO flags
  // //

  // MBO 20160928: Bring up all slinks in U1
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000009 , 0x000F ); // SLINK_CTL2_REG
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000009 , 0x000F ); // SLINK_CTL2_REG
  // MBO 20160928: Set all slinks to data dump mode in U1:
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU1 , FLIC::cmdSet , 0x00000009 , 0x00F0 ); // SLINK_CTL2_REG
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU2 , FLIC::cmdSet , 0x00000009 , 0x00F0 ); // SLINK_CTL2_REG

  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdSet , 0x0000020F , 0x0001 ); // Pulse the reset bit.
  // MBO: Register 0x6a contain the 8 channel processing enables bits 7:0 , and the master processing enabled bit 8.
  // This also sets which channels are expected to report fragments.
  // Take U4's Tagged Event Receivers out of reset.
  // SendSet( targetFpgaU4 , 0x0000006A , 0x0101 ); // U1-ch1
  // SendSet( targetFpgaU4 , 0x0000006A , 0x0103 ); // U1-ch1+ch2
  bytes += flic_proc->singlewrite( FLIC::targetFpgaU4 , FLIC::cmdSet , 0x0000006A , 0x010F ); // U1-all
  // SendSet( targetFpgaU4 , 0x0000006A , 0x01F0 ); // U2-all

  return bytes;

}
