#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flic1 , flic_comm *flic2 , FLICController ctrl );

int main() {
  int bytes = 0;
  FLICController ctrl;

  while( true ) {

    cout << endl;
    cout << "--- Board #1 ---" << endl;
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic1 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );

    cout << endl;
    cout << "--- Board #2 ---" << endl;
    b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic2 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    
    bytes += Option( flic1 , flic2 , ctrl );

    delete flic1;
    delete flic2;
  }
    
  return bytes;
}


int Option( flic_comm *flic1 , flic_comm *flic2 , FLICController ctrl ) {
  int option = 1;
  int bytes  = 0;
  while( option != 0 ) {
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On" << endl
	 << " 100 Power Off" << endl
	 << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
	 << " Basic initialization controls..." << endl
	 << "   2 Program All FPGAs" << endl
	 << "   3 Setup" << endl
	 << "   4 Send sync event from SSBe board to Proc board" << endl
	 << "   5 Reset SSBe" << endl
         << standalone::line << endl
	 << "  77 Start SSBe data" << endl
	 << "  78 Stop SSBe data" << endl
	 << standalone::line << endl
	 << " Diagnostics..." << endl
	 << "  84 Reset and arm monitoring FIFOs" << endl
	 << "  85 Dump monitoring FIFOs on Flic1" << endl
	 << "  86 Dump monitoring FIFOs on Flic2" << endl
	 << "  90 Initialize counters" << endl
	 << "  91 Dump counters" << endl
	 << " 222 Check program status" << endl
	 << " 223 Check clock counters" << endl
	 << " 224 Check XOFF counters" << endl
	 << " 225 Change to \"dump data on the floor\" mode" << endl
	 << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) {
      bytes += ctrl.PICPowerOn( flic1 );
      bytes += ctrl.PICPowerOn( flic2 );
    }
    if (option == 100) {
      bytes += ctrl.PICPowerOff( flic1 );
      bytes += ctrl.PICPowerOff( flic2 );
    }
    if (option == 200) {
      bytes+=ctrl.PICPowerFromIPMC( flic1 );
      bytes+=ctrl.PICPowerFromIPMC( flic2 );
    }
    if (option == 2) {
      // Program proc board
      ctrl.ProgramFPGAs( flic1 );
      ctrl.ProgramFPGAs( flic2 );
      assert( ctrl.CheckClockCounters( flic1 , FLIC::targetFpgaU1 ) );
      assert( ctrl.CheckClockCounters( flic2 , FLIC::targetFpgaU2 ) );
    }
    if (option == 3) {
      /*
	bytes += standalone::SetupSSBeAndProc( flic1 , FLIC::targetFpgaU2 , flic2 , FLIC::targetFpgaU1 );
	bytes += standalone::SetupSSBeAndProc( flic2 , FLIC::targetFpgaU2 , flic1 , FLIC::targetFpgaU1 );
	bytes += standalone::ResetU3U4( flic1 );
	bytes += standalone::ResetU3U4( flic2 );
      */
      assert( false );
    }
    if (option == 4) {
      bytes += ctrl.StartSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
      bytes += ctrl.StartSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf , 1 , 20 , 4000 );
    }
    if (option == 5) {
      bytes += ctrl.ResetSSBe( flic1 , FLIC::targetFpgaU2 );
      bytes += ctrl.ResetSSBe( flic2 , FLIC::targetFpgaU2 );
    }
    if (option == 77) {
      bytes += ctrl.StartSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.StartSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf , 0 , -1 , -1 , false );
      bytes += ctrl.RestartSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf );
      bytes += ctrl.RestartSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 78) {
      bytes += ctrl.StopSSBeData( flic1 , FLIC::targetFpgaU2 , 0xf );
      bytes += ctrl.StopSSBeData( flic2 , FLIC::targetFpgaU2 , 0xf );
    }
    if (option == 84) {
      bytes += ctrl.ResetArmMonitoringFIFOs( flic1 , FLIC::targetFpgaU1 , 0 );
      bytes += ctrl.ResetArmMonitoringFIFOs( flic2 , FLIC::targetFpgaU1 , 0 );
    }
    if (option == 85) bytes += standalone::ReadMonitoringFIFOs( flic1 , FLIC::targetFpgaU1 );
    if (option == 86) bytes += standalone::ReadMonitoringFIFOs( flic2 , FLIC::targetFpgaU1 );
    if (option == 90) {
      bytes += ctrl.InitProcCounters( flic1 , FLIC::targetFpgaU1 , 0x0 );
      bytes += ctrl.InitProcCounters( flic2 , FLIC::targetFpgaU1 , 0x0 );
    }      
    if (option == 91) {
      bytes += standalone::DumpProcCounters( flic1 , FLIC::targetFpgaU1 );
      bytes += standalone::DumpProcCounters( flic2 , FLIC::targetFpgaU1 );
    }
    if (option == 222) {
      cout << "--- Board #1 ---" << endl
	   << ctrl.CheckStatus( flic1 ) << endl
	   << "--- Board #2 ---" << endl
	   << ctrl.CheckStatus( flic2 ) << endl;
    }
    if (option == 223) {
      cout << ctrl.CheckClockCounters( flic1 , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckClockCounters( flic2 , FLIC::targetFpgaU1 ) << endl;
    }
    if (option == 224) {
      cout << ctrl.CheckXOFFCounters( flic1 , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckXOFFCounters( flic2 , FLIC::targetFpgaU1 ) << endl;
    }
    if (option == 225) {
      bytes += ctrl.DumpDataOnTheFloorMode( flic1 , FLIC::targetFpgaU1 );
      bytes += ctrl.DumpDataOnTheFloorMode( flic2 , FLIC::targetFpgaU1 );
    }
    //option=0;//One time!
  }//while option
  return bytes;
}



