#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flictalk , FLICController ctrl );


int main() {
  int bytesent = 0;
  FLICController ctrl;

  while( true ) {
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic1 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    bytesent += Option( flic1 , ctrl );
    delete flic1; // sockets closed in destructor
  }
    
  return bytesent;
}


int Option( flic_comm *flictalk , FLICController ctrl ) {
  int option=1;
  int bytesent=0;
  while(option!=0){
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On                      100 Power Off" << endl
	 << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
	 << " Basic initialization controls..." << endl
         << "   2 Program All FPGAs               3 Idle Board" << endl
         << "   4 Setup One Channel Proc.         5 Send sync word to SSB" << endl
         << "   6 Reset SSBe                     20 Send single event from SSBe on all channels" << endl
         << standalone::line << endl
	 << "  77 Start SSBe data with new configuration" << endl
	 << "  78 Stop SSBe data but leave configuration unchanged" << endl
         << standalone::line << endl
         << "   7 Read a register                 8 Write a register" << endl
         << "   9 Download Firmware" << endl
         << standalone::line << endl
         << "  11 Pulse flow control to SSB      12 Reset S-Link" << endl
         << "  13 Change Emulation Parameters" << endl
         << standalone::line << endl
	 << " Diagnostics..." << endl
	 << "  90 Init counters to COUNT MODE    91 Init counters to RATE MODE" << endl
	 << "  92 Dump counters" << endl
      	 << " 222 Check program status          223 Check U2 clock counters" << endl
	 << " 224 Check XOFF counters           225 Change to \"dump data on the floor\" mode" << endl
	 << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) bytesent+=ctrl.PICPowerOn( flictalk );
    if (option == 100) bytesent+=ctrl.PICPowerOff( flictalk );
    if (option == 200) bytesent+=ctrl.PICPowerFromIPMC( flictalk );
    if (option == 2) {
      while( true ) {
	// continue to re-program until all clocks come up correctly
	ctrl.ProgramFPGAs( flictalk , FLIC::ListProc::Loopback_Reverse_NoSRAMs );
	if( ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU2 ) ) break;
	cout << endl;
	ctrl.PICPowerOff( flictalk );
	usleep( 100000 );
	ctrl.PICPowerOn( flictalk );
      }
    }
    if (option == 3) {
      bytesent += ctrl.ResetSERDES_Proc( flictalk , FLIC::targetFpgaU2 );
      bytesent += ctrl.ResetSERDES_SSBe( flictalk , FLIC::targetFpgaU1 );
    }
    if (option == 4) {
      bytesent += ctrl.InitStateMachines_Proc( flictalk , FLIC::targetFpgaU2 );
      bytesent += ctrl.InitStateMachines_SSBe( flictalk , FLIC::targetFpgaU1 );
    }
    if (option == 5) bytesent += ctrl.SyncWithSSB( flictalk , FLIC::targetFpgaU2 );
    if (option == 6) bytesent += ctrl.ResetSSBe( flictalk , FLIC::targetFpgaU1 );
    if (option == 20) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 , 0xf , 1 , 20 , 4000 );
    if (option == 77) bytesent += ctrl.StartSSBeData( flictalk , FLIC::targetFpgaU1 );
    if (option == 78) bytesent += ctrl.StopSSBeData( flictalk , FLIC::targetFpgaU1 , 0xf );
    if (option == 7) bytesent += standalone::ReadRegister( flictalk );
    if (option == 8) bytesent += standalone::WriteRegister( flictalk );
    if (option == 9) bytesent += standalone::DownloadFirmwareToFlash( flictalk );
    if (option == 11) bytesent += ctrl.PulseFlowControlToSSB( flictalk , FLIC::targetFpgaU2 );
    if (option == 12) bytesent += ctrl.ResetSLinks( flictalk , FLIC::targetFpgaU2 );
    if (option == 13) bytesent += standalone::ChangeSSBeRunParam( flictalk , FLIC::targetFpgaU1 );
    if (option == 90) bytesent += ctrl.InitProcCounters( flictalk , FLIC::targetFpgaU2 , 0x0000 );
    if (option == 91) bytesent += ctrl.InitProcCounters( flictalk , FLIC::targetFpgaU2 , 0xFFFF );
    if (option == 92) bytesent += standalone::DumpProcCounters( flictalk , FLIC::targetFpgaU2 );
    if (option == 222) cout << ctrl.CheckStatus( flictalk ) << endl; 
    if (option == 223) cout << ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU2 ) << endl;
    if (option == 224) cout << ctrl.CheckXOFFCounters( flictalk , FLIC::targetFpgaU2 ) << endl;
    if (option == 225) bytesent += ctrl.DumpDataOnTheFloorMode( flictalk , FLIC::targetFpgaU2 );
    //option=0;//One time!
  }//while option
  return bytesent;
}


