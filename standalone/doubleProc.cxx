#include <iostream>
#include <iomanip>
#include <string>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>
#include <sstream>

#include "flic/StandaloneTools.h"
#include "flic/FLICController.h"
#include "flic/flic_comm.h"
#include "flic/FLIC.h"

using namespace daq::ftk;
using namespace std;

//
// Extra functions used only in flicscope, implemented after "main"
//
//   IMPORTANT! - If you want to add functions that will be shared
//  by aFLICtion and/or ReadoutModule_flic, please add them to the
//  FLICController class instead of adding them here!
//
int Option( flic_comm *flictalk , FLICController ctrl );


int main() {
  int bytesent = 0;
  FLICController ctrl;

  while( true ) {
    FLIC::Comm::Info b = standalone::getFlicSelectionFromUser();
    if( ! b.isDefined ) break;
    flic_comm *flic1 = new flic_comm( b.boardIP.c_str() , b.port.c_str() , b.hostIP.c_str() );
    bytesent += Option( flic1 , ctrl );
    delete flic1; // sockets closed in destructor
  }
    
  return bytesent;
}


int Option( flic_comm *flictalk , FLICController ctrl ) {
  int option=1;
  int bytesent=0;
  while(option!=0){
    cout << endl
         << "Please select an option (0 to Exit):" << endl
         << "   1 Power On" << endl
	 << " 100 Power Off" << endl
	 << " 200 Power control from IPMC (not yet working at ANL)" << endl
         << standalone::line << endl
	 << " Basic initialization controls..." << endl
	 << "   2 Program All FPGAs" << endl
	 << "   3 Initialize links and state machines" << endl
         << standalone::line << endl
         << "   7 Read a register" << endl
	 << "   8 Write a register" << endl
         << standalone::line << endl
         << "  11 Pulse flow control to SSB" << endl
	 << "  12 Reset S-Link" << endl
         << standalone::line << endl
	 << " Diagnostics..." << endl
	 << "  74 Reset and arm monitoring FIFOs" << endl
	 << "  75 Dump monitoring FIFOs on U1" << endl
	 << "  76 Dump monitoring FIFOs on U2" << endl
	 << "  90 Initialize counters" << endl
	 << "  91 Dump counters" << endl
      	 << " 222 Check program status" << endl
	 << " 223 Check clock counters" << endl
	 << " 224 Check XOFF counters" << endl
	 << " 225 Change to \"dump data on the floor\" mode" << endl
	 << standalone::line << endl
         << "Your choice: ";

    scanf("%d",&option);
    if (option == 1) bytesent+=ctrl.PICPowerOn( flictalk );
    if (option == 100) bytesent+=ctrl.PICPowerOff( flictalk );
    if (option == 200) bytesent+=ctrl.PICPowerFromIPMC( flictalk );
    if (option == 2) {
      while( true ) {
	// continue to re-program until all clocks come up correctly
	ctrl.ProgramFPGAs( flictalk , FLIC::ListProc::DoubleProc_NoSRAMs );
	bool U1good = ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU1 ); cout << endl;
	bool U2good = ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU2 ); cout << endl;
	if( U1good && U2good ) break;
	usleep( 100000 );
      }
    }
    if (option == 3) {
      bytesent += ctrl.ResetSERDES_Proc( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.ResetSERDES_Proc( flictalk , FLIC::targetFpgaU2 );
      bytesent += ctrl.InitStateMachines_Proc( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.InitStateMachines_Proc( flictalk , FLIC::targetFpgaU2 );
    }
    if (option == 7) bytesent += standalone::ReadRegister( flictalk );
    if (option == 8) bytesent += standalone::WriteRegister( flictalk );
    if (option == 11) {
      bytesent += ctrl.PulseFlowControlToSSB( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.PulseFlowControlToSSB( flictalk , FLIC::targetFpgaU2 );
    }
    if (option == 12) {
      bytesent += ctrl.ResetSLinks( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.ResetSLinks( flictalk , FLIC::targetFpgaU2 );
    }
    if (option == 74) {
      bytesent += ctrl.ResetArmMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 , 0 );
      bytesent += ctrl.ResetArmMonitoringFIFOs( flictalk , FLIC::targetFpgaU2 , 0 );
    }
    if (option == 75) bytesent += standalone::ReadMonitoringFIFOs( flictalk , FLIC::targetFpgaU1 );
    if (option == 76) bytesent += standalone::ReadMonitoringFIFOs( flictalk , FLIC::targetFpgaU2 );
    if (option == 90) {
      bytesent += ctrl.InitProcCounters( flictalk , FLIC::targetFpgaU1 , 0x0 );
      bytesent += ctrl.InitProcCounters( flictalk , FLIC::targetFpgaU2 , 0x0 );
    }      
    if (option == 91) {
      bytesent += standalone::DumpProcCounters( flictalk , FLIC::targetFpgaU1 );
      bytesent += standalone::DumpProcCounters( flictalk , FLIC::targetFpgaU2 );
    }
    if (option == 222) cout << ctrl.CheckStatus( flictalk ) << endl; 
    if (option == 223) {
      cout << ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckClockCounters( flictalk , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 224) {
      cout << ctrl.CheckXOFFCounters( flictalk , FLIC::targetFpgaU1 ) << endl;
      cout << ctrl.CheckXOFFCounters( flictalk , FLIC::targetFpgaU2 ) << endl;
    }
    if (option == 225) {
      bytesent += ctrl.DumpDataOnTheFloorMode( flictalk , FLIC::targetFpgaU1 );
      bytesent += ctrl.DumpDataOnTheFloorMode( flictalk , FLIC::targetFpgaU2 );
    }
    //option=0;//One time!
  }//while option
  return bytesent;
}

