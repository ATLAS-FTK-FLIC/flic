#ifndef READOUT_MODULE_BLADE_H
#define READOUT_MODULE_BLADE_H

class ReadoutModule_blade: public ROS::ReadoutModule
{
 private:
  // objet Eth connection
  struct sockaddr_in FLIC1_addr;
  int FLIC1_sockfd;
  
  // object buffer
  uint16_t mesg[MAXLINE];

  // object data container
  bladeData vector blade data;

  // object plots;
  a object of all the plots for OH;


 public: 
  ReadoutModule_blade();
  // set default parameter values

  connect();
  // set up blade to prepare to receive

  receive();
  // receive data and put them in the buffer

  collect();
  // convert the data in buffer to data container

  makeplots();
  // make plots from the data container

  publishtoOH();
  // publish histograms to OH

  ~ReadoutModule_blade();
  // disconnect 
  // clear all data
  // close socket





#endif // READOUT_MODULE_BLADE_H
