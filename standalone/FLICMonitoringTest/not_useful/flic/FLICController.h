/*****************************************************************/
//  Class: FLICController                                         
//  Synopsis: This is an object that controls FLIC. It has every
//            necessary data member and data functin here
//  Author:  Jordan Webster jwebster@anl.gov                     
//  Created: 2016.02.08                                          
//  Modification:
//        2016.03.03 by Peilong Wang (wplong.work@gmail.com):
//          delete flic_comm.h & flic_comm.cxx
//          move all the flic communication method into this object
//          rewrite FLICController
/*****************************************************************/

#ifndef FLICController_H
#define FLICController_H

#include <stdio.h> // printf()
#include <sys/types.h>
#include <sys/socket.h> // socket() function
#include <netinet/in.h> // struct sockaddr_in

#include <arpa/inet.h> // htonl() 
#include <strings.h> // bzero()



#include "FLIC.h" // using FLIC object

namespace daq {
  namespace ftk {

    class FLICController {
      
   private:

      struct sockaddr_in FLICController_addr;
      struct sockaddr_in FLIC_addr;
      int sockfd;
      
   public:
      FLICController(){}
      //      ~FLICController() {}

      int set_FLICController_addr(uint16_t port_number,char *internet_addr){

        // initialize FLICController_addr to 0
        bzero(&FLICController_addr, sizeof(FLICController_addr));

        FLICController_addr.sin_family = AF_INET; // AF_INET -> IPv4
        // This function converts the character string src into a network
        // address structure in the af address family, then copies the network
        // address structure to dst.  The af argument must be either AF_INET or
        // AF_INET6.  dst is written in network byte order.
        inet_pton(AF_INET, internet_addr, &FLICController_addr.sin_addr); 
        // #include <arpa/inet.h> The htons() function converts
        // the unsigned short integer hostshort from host byte order
        // to network byte order.          
        FLICController_addr.sin_port = htons(port_number);

        return 0;
      }


     
      struct sockaddr_in get_FLICController_addr() {
        return FLICController_addr;
      }
      

      int set_sockfd_by_opening_socket_on_controller_and_binding() {
        sockfd = socket(AF_INET, SOCK_DGRAM, 0);
        bind(sockfd, (struct sockaddr *) &FLICController_addr, sizeof(FLICController_addr));
        
        return 0;
      }

      int get_sockfd() {
        return sockfd;
      }

      int send_FLIC_UDP_packet() {
        bzero(&FLIC_addr, sizeof(FLIC_addr));
        FLIC_addr.sin_family = AF_INET;
        FLIC_addr.sin_port = htons(50000);
        inet_pton(AF_INET, "10.193.32.18", &FLIC_addr.sin_addr);
  
        sendto(sockfd, "hello FLIC", 10, 0, (struct sockaddr *) &FLIC_addr, sizeof(FLIC_addr));                      

        return 0;
      }



          
    }; // end of class FLICController


  }  // end of namespace ftk

}  // end of namespace daq


#endif
