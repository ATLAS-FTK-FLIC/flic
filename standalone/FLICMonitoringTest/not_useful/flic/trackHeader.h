#ifndef TRACK_HEADER_H
#define TRACK_HEADER_H

#include <sys/types.h>

class TrackHeader {
private:
  uint16_t resLBDA;
  uint16_t sectorNumber;
  uint16_t trackFilterNumberAndTowernumber;
  uint16_t layerMap;
  uint16_t roadIDBit23to16;
  uint16_t roadIDBit15to0;
  uint16_t trackChisq;
  uint16_t trackD0;
  uint16_t trackZ0;
  uint16_t trackCotth;
  uint16_t trackPhi0;
  uint16_t trackCurv;

public:
  TrackHeader();

  uint16_t getResLBDA() const;
  int setResLBDA(uint16_t);
  uint16_t getSectorNumber() const;
  int setSectorNumber(uint16_t); 
  uint16_t getTrackFilterNumberAndTowernumber() const;
  int setTrackFilterNumberAndTowernumber(uint16_t);
  uint16_t getLayerMap() const;
  int setLayerMap(uint16_t);
  uint16_t getRoadIDBit23to16() const;
  int setRoadIDBit23to16(uint16_t);
  uint16_t getRoadIDBit15to0() const;
  int setRoadIDBit15to0(uint16_t);
  uint16_t getTrackChisq() const;
  int setTrackChisq(uint16_t);
  uint16_t getTrackD0() const;
  int setTrackD0(uint16_t);
  uint16_t getTrackZ0() const;
  int setTrackZ0(uint16_t);
  uint16_t getTrackCotth() const;
  int setTrackCotth(uint16_t);
  uint16_t getTrackPhi0() const;
  int setTrackPhi0(uint16_t);
  uint16_t getTrackCurv() const;
  int setTrackCurv(uint16_t);

  ~TrackHeader();
}

#endif TRACK_HEADER_H
      
