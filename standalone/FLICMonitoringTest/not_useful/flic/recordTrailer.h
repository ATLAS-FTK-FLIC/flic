#ifndef RECORD_TRAILER_H
#define RECORD_TRAILER_H

#include <sys/types.h>

class RecordTrailer {
private:
  uint16_t e0da;
  uint16_t lengthOfDebugBlockBeginning;
  vector<uint16_t> debugInformation;
  uint16_t e0df;
  uint16_t lengthOfDebugBlockEnd;
  uint16_t level1IDBit31to16InTrailer;
  uint16_t level1IDBit15to0InTrailer;
  uint16_t errorFlagBit31to16;
  uint16_t errorFlagBit15to0;
  uint16_t reserved1;
  uint16_t reserved2;
  uint16_t reserved3;
  uint16_t reserved4;

public:
  
