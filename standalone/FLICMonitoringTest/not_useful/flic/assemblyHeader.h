#ifndef ASSEMBLY_HEADER_H
#define ASSEMBLY_HEADER_H

#include <sys/types.h>

class AssemblyHeader {
private:
  uint16_t level1ID;
  uint16_t nOfWordsFollowingThisWord;
  uint16_t bitmask;
  uint16_t fragmentID0123;
  uint16_t fragmentID4567;

public:

  AssemblyHeader();

  uint16_t getLevel1ID() const;
  int setLevel1ID(uint16_t);
  uint16_t getNOfWordsFollowingThisWord() const;
  int setNOfWordsFollowingThisWord(uint16_t);
  uint16_t getBitmask() const;
  int setBitmask(uint16_t);
  uint16_t getFragmentID0123() const;
  int setFragmentID0123(uint16_t);
  uint16_t getFragmentID4567() const;
  int setFragmentID4567(uint16_t);

  ~AssemblyHeader();
}

#endif ASSEMBLY_HEADER_H
