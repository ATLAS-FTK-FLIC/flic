#ifndef TRACK_DATA_H
#define TRACK_DATA_H

#include <sys/types.h>

class TrackData {
private:
  uint16_t iblColWidthAndCoordinate;
  uint16_t iblRowWidthAndCoordinate;
  uint16_t pl0ColWidthAndCoordinate;
  uint16_t pl0RowWidthAndCoordinate;
  uint16_t pl1ColWidthAndCoordinate;
  uint16_t pl1RowWidthAndCoordinate;
  uint16_t pl2ColWidthAndCoordinate;
  uint16_t pl2RowWidthAndCoordinate;

  uint16_t sax0WidthAndCoordinate;
  uint16_t sat0WidthAndCoordinate;
  uint16_t sax1WidthAndCoordinate;
  uint16_t sat1WidthAndCoordinate;
  uint16_t sax2WidthAndCoordinate;
  uint16_t sat2WidthAndCoordinate;
  uint16_t sax3WidthAndCoordinate;
  uint16_t sat3WidthAndCoordinate;

public:

  TrackData();
  
  int setIblColWidthAndCoordinate(uint16_t);
  uint16_t getIblColWidthAndCoordinate() const;
  int setIblRowWidthAndCoordinate(uint16_t);
  uint16_t getIblRowWidthAndCoordinate() const;
  int setPl0ColWidthAndCoordinate(uint16_t);
  uint16_t getPl0ColWidthAndCoordinate() const;
  int setPl0RowWidthAndCoordinate(uint16_t);
  uint16_t getPl0RowWidthAndCoordinate() const;
  int setPl1ColWidthAndCoordinate(uint16_t);
  uint16_t getPl1ColWidthAndCoordinate() const;
  int setPl1RowWidthAndCoordinate(uint16_t);
  uint16_t getPl1RowWidthAndCoordinate() const;
  int setPl2ColWidthAndCoordinate(uint16_t);
  uint16_t getPl2ColWidthAndCoordinate() const;
  int setPl2RowWidthAndCoordinate(uint16_t);
  uint16_t getPl2RowWidthAndCoordinate() const;

  int setSax0WidthAndCoordinate(uint16_t);
  uint16_t getSax0WidthAndCoordinate() const;
  int setSat0WidthAndCoordinate(uint16_t);
  uint16_t getSat0WidthAndCoordinate() const;
  int setSax1WidthAndCoordinate(uint16_t);
  uint16_t getSax1WidthAndCoordinate() const;
  int setSat1WidthAndCoordinate(uint16_t);
  uint16_t getSat1WidthAndCoordinate() const;
  int setSax2WidthAndCoordinate(uint16_t);
  uint16_t getSax2WidthAndCoordinate() const;
  int setSat2WidthAndCoordinate(uint16_t);
  uint16_t getSat2WidthAndCoordinate() const;
  int setSax3WidthAndCoordinate(uint16_t);
  uint16_t getSax3WidthAndCoordinate() const;
  int setSat3WidthAndCoordinate(uint16_t);
  uint16_t getSat3WidthAndCoordinate() const;

  ~TrackData();
}

#endif TRACK_DATA_H
