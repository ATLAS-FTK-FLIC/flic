#ifndef RECORD_HEADER_H
#define RECORD_HEADER_H

#include <sys/types.h>

class RecordHeader {
private:
  uint16_t runNumberBit31to16;
  uint16_t runNumberBit15to0;
  uint16_t extendedLevel1IDBit31to16;
  uint16_t extendedLevel1IDBit15to0;
  uint16_t recordHeaderLine9Reserved;
  uint16_t reservedAndBCID;
  uint16_t recordHeaderLine11Reserved;
  uint16_t level1TriggerType;
  uint16_t detectorEventType;
  uint16_t reservedAndTIM;

public:
  RecordHeader();

  uint16_t getRunNumberBit31to16() const;
  int setRunNumberBit31to16(uint16_t);
  uint16_t getRunNumberBit15to0() const;
  int setRunNumberBit15to0(uint16_t);
  uint16_t getExtendedLevel1IDBit31to16() const;
  int setExtendedLevel1IDBit31to16(uint16_t);
  uint16_t getExtendedLevel1IDBit15to0() const;
  int setExtendedLevel1IDBit15to0(uint16_t);
  uint16_t getRecordHeaderLine9Reserved() const;
  int setRecordHeaderLine9Reserved(uint16_t);
  uint16_t getReservedAndBCID() const;
  int setReservedAndBCID(uint16_t);
  uint16_t getRecordHeaderLine11Reserved() const;
  int setRecordHeaderLine11Reserved(uint16_t);
  uint16_t getLevel1TriggerType() const;
  int setLevel1TriggerType(uint16_t);
  uint16_t getDetectorEventType() const;
  int setDetectorEventType(uint16_t);
  uint16_t getReservedAndTIM() const;
  int setReservedAndTIM(uint16_t);  

  ~RecordHeader();
}

#endif RECORD_HEADER_H
