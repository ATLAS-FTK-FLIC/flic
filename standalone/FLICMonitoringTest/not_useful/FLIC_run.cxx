#include "flic/FLICController.h"

using namespace daq::ftk;

int main(int argc, char *argv[])
{

  char FLICController_IP[13] = "10.193.32.17";
  
  FLICController USTB14;
  USTB14.set_FLICController_addr(50000, FLICController_IP);
  USTB14.set_sockfd_by_opening_socket_on_controller_and_binding();
  USTB14.send_FLIC_UDP_packet();
  
  return 0;
}
