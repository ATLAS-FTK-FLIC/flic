#include <stdio.h> // printf()
#include <sys/types.h>
#include <sys/socket.h> // socket() function
#include <netinet/in.h> // struct sockaddr_in

#include <arpa/inet.h> // htonl() 
#include <strings.h> // bzero()

//#include <sys/types.h>

#define MAXLINE 4096

void dg_echo(int sockfd, struct sockaddr *pcliaddr, socklen_t clilen)                  
{                                                                          int n;                                                
  socklen_t len;                                                      
  char mesg[MAXLINE];                                            

  printf("print something before the infinite for loop\n");              

  for ( ; ; ) {                                                         
    len = clilen;
    n = recvfrom(sockfd, mesg, MAXLINE, 0, pcliaddr, &len);       
    printf("Received---> %s\n", mesg);      
    sendto(sockfd, mesg, n, 0, pcliaddr, len);                      
  }                                                                     
}




int main(int argc, char **argv)
{
  int sockfd;
  // struct sockaddr_in is from <netinet/in.h>
  struct sockaddr_in servaddr, cliaddr; 

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);

  bzero(&servaddr, sizeof(servaddr));

  servaddr.sin_family = AF_INET;
  // #include <arpa/inet.h>  The htonl() function converts the unsigned integer hostlong from host byte order to network byte order. 
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

  // #include <arpa/inet.h> The htons() function converts the unsigned short integer hostshort from host byte order to network byte order. 
  servaddr.sin_port = htons(50001); //SERV_PORT);

  // bind socket with address
  bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

  printf("------> a mark before dg_echo() \n");

  dg_echo(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr));
}
