#include <stdio.h> // printf()
#include <sys/types.h>
#include <sys/socket.h> // socket() function
#include <netinet/in.h> // struct sockaddr_in

#include <arpa/inet.h> // htonl() 
#include <strings.h> // bzero()

//#include <sys/types.h>

#define MAXLINE 4096


void dg_cli(FILE *fp, int sockfd, const struct sockaddr *pservaddr, socklen_t servlen)
{
  int n;
  char sendline[MAXLINE], recvline[MAXLINE + 1];
  
  while (fgets(sendline, MAXLINE, fp) != NULL) {
    sendto(sockfd, sendline, strlen(sendline), 0, pservaddr, servlen);
    n = recvfrom(sockfd, recvline, MAXLINE, 0, NULL, NULL);
    recvline[n] = 0;	/* null terminate */
    fputs(recvline, stdout);
  }
}


int main(int argc, char **argv)
{
  int sockfd;
  struct sockaddr_in servaddr, cliaddr;
  
  if (argc != 2) {
    perror("usage: udpcli <IPaddress>");
    exit(1);
  }

  bzero(&servaddr, sizeof(servaddr));
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(50001);//SERV_PORT);
  inet_pton(AF_INET, argv[1], &servaddr.sin_addr);
  

  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  bzero(&cliaddr, sizeof(cliaddr));

  cliaddr.sin_family = AF_INET;
  cliaddr.sin_addr.s_addr = htonl(INADDR_ANY);
  cliaddr.sin_port = htons(50000); //CLI_PORT);

  bind(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr));

  
  dg_cli(stdin, sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));

  exit(0);
}
