#ifndef FLIC_MON_APP_H
#define FLIC_MON_APP_H
//////////////////////////////////////// for debugging
#include <iostream>
#include <iterator>

//////////////////////////////////////// C library
#include <stdio.h>   // printf()
#include <sys/types.h>
#include <sys/socket.h>   // socket() function
#include <netinet/in.h>   // struct sockaddr_in
#include <arpa/inet.h>   // htonl()


//////////////////////////////////////// C++ library
#include <vector>
#include <deque>

//////////////////////////////////////// ROOT library

#include "TH1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"


//////////////////////////////////////// Constant definition
#define MAXLINE 750 // Max word received from FLIC: 5000 words
#define SPYBUFFERSIZE 10 // How many events stored in spy buffer
#define HALFEVENTSIZE 7 // How many UDP packets are able to be stored in h \


//////////////////////////////////////// Struct definition
struct FLIC_UDP_packet {
  uint16_t mesg[MAXLINE];  // uint16_t -> unsigned short int
};

struct assembly_fragment_position {
  uint16_t * beginning_position;
  std::vector<uint16_t *> ET01_position;
  uint16_t * end_position;
};


//////////////////////////////////////// Class definition
class FLIC_Mon_App {

private:
  //////////////////// declare of storage variable in the blade memory
  std::vector<FLIC_UDP_packet> flic_udp_buffer;
  uint16_t * mesg_ptr = flic_udp_buffer.at(0).mesg;

  std::deque<assembly_fragment_position> events_position;

  
  /*
  std::vector<struct one_event> * events;
  FLIC_UDP_packet mesg_buffer;
  */
  
  //////////////////// declare of socket variables
  struct sockaddr_in FLIC1_Spybuffer_addr;
  struct sockaddr_in blade_ETH_addr;
  int FLIC1_Spybuffer_sockfd;
  int blade_ETH_sockfd;
  socklen_t addr_len;
  struct sockaddr * addr_ptr;


  //////////////////// declare of histograms
  TH1F *m_hTrackChi2_Blade;
  TH1F *m_hTrackD0_Blade;
  TH1F *m_hTrackZ0_Blade;
  TH1F *m_hTrackCotth_Blade;
  TH1F *m_hTrackPhi_Blade;
  TH1F *m_hTrackCurv_Blade;
  

public:
  FLIC_Mon_App();

  void setup();

  void prepareForRun();

  int mark_ET01_position(std::deque<assembly_fragment_position>::iterator, uint16_t *, uint16_t *);

  int checkEvent(std::deque<assembly_fragment_position>::iterator evt);
  
  ~FLIC_Mon_App();
  
};

#endif // FLIC_MON_APP_H

