// "Copyright [2017] <Copyright Owner:Peilong Wang>"

#include <stdio.h>   // printf()
#include <sys/types.h>
#include <sys/socket.h>   // socket() function
#include <netinet/in.h>   // struct sockaddr_in

#include <arpa/inet.h>   // htonl()

////////////////////////
#include "TH1.h"
#include "TCanvas.h"
#include "TStyle.h"
#include "TROOT.h"

//#include <string>
//#include <strings.h>  // bzero()

// #include <sys/types.h>

#define MAXLINE 3000 // Max word received from FLIC: 5000 words
//#define bzero(s, n) memset((s), 0, (n))

void float32(float* __restrict out, const uint16_t in) {
  uint32_t t1;
  uint32_t t2;
  uint32_t t3;

  t1 = in & 0x7fff;                       // Non-sign bits
  t2 = in & 0x8000;                       // Sign bit
  t3 = in & 0x7c00;                       // Exponent

  t1 <<= 13;                              // Align mantissa on MSB
  t2 <<= 16;                              // Shift sign bit into position

  t1 += 0x38000000;                       // Adjust bias

  t1 = (t3 == 0 ? 0 : t1);                // Denormals-as-zero

  t1 |= t2;                               // Re-insert sign bit

  *((uint32_t*)out) = t1;
};


void dg_echo(int sockfd, struct sockaddr *pcliaddr, socklen_t clilen, TH1F* h1) {
  
  int n;
  socklen_t len;
  uint16_t mesg[MAXLINE][MAXLINE] = {0};  // uint16_t -> unsigned short int

  int i;

  float temp = 0;
  //  printf("print something before the infinite for loop\n");
  
  len = clilen;
  n = recvfrom(sockfd, mesg, MAXLINE, 0, pcliaddr, &len);
  
  for (i=0; i<MAXLINE; ++i) {
    if (mesg[i] == 3034) {
      float32(&temp, mesg[i+8]);
      printf("temp float: %f\n", temp);
      h1->Fill(temp * 100);
    }
  }
  
  //    fputs(mesg, stdout);
  /*
  printf("Received--->\n");
  for (i = 0; i < 1500 ; ++i )
    printf("----------->%04x\n", mesg[i]);
 

  printf("******************end of UDP packet**********************\n");
  */
  /*
  // print data to file
  FILE *f = fopen("vector_from_flic.txt", "w");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }

  // print data to file 
  for (i = 0; i < 1500; ++i)
    fprintf(f, "%04x\n", mesg[i]);

  fclose(f);
  */
      
  //  sendto(sockfd, mesg, n, 0, pcliaddr, len);
}




int main(int argc, char **argv) {
  int sockfd;
  // struct sockaddr_in is from <netinet/in.h>
  struct sockaddr_in servaddr, cliaddr;

  int udp_packet_counter = 0;

  TCanvas* c1 = new TCanvas("c1","",800,800);
  TH1F* h1 = new TH1F("h1","",100,-800,800);

  c1->cd();
  
  int i = 0;
  
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);

  //  bzero(&servaddr, sizeof(servaddr));

  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;

  // #include <arpa/inet.h>  The htonl() function converts
  // the unsigned integer hostlong from host byte order to network byte order.
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

  // #include <arpa/inet.h> The htons() function converts
  // the unsigned short integer hostshort from host byte order
  // to network byte order.
  servaddr.sin_port = htons(50001);  // SERV_PORT);

  // bind socket with address
  bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));



  if (argc != 2) {
    perror("usage: udpserv <IPaddress> -- give client IP address");
    exit(1);
  }



  memset(&cliaddr, 0, sizeof(cliaddr));

  cliaddr.sin_family = AF_INET;
  // convert IPv4 and IPv6 addresses from text to binary form
  inet_pton(AF_INET, argv[1], &cliaddr.sin_addr);
  cliaddr.sin_port = htons(50000);   // CLI_PORT);


  printf("------> a mark before dg_echo() \n");
  
  for(udp_packet_counter = 1; ;  ++ udp_packet_counter) {
    if (udp_packet_counter%1000 == 0)
      printf("udp_packet_counter: %d\n", udp_packet_counter);
    dg_echo(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr), h1);

    if (udp_packet_counter % 100 == 0) {
      h1->GetXaxis()->SetTitle("Track Z0 / mm");
      h1->GetYaxis()->SetTitle("counts");
      h1->Draw();
      c1->SaveAs("last_100_udp_packtets.png");
    }       
  }

  return 0;
}
