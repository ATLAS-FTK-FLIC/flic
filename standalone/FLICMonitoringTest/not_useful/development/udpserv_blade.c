// "Copyright [2017] <Copyright Owner:Peilong Wang>"

#include <stdio.h>   // printf()
#include <sys/types.h>
#include <sys/socket.h>   // socket() function
#include <netinet/in.h>   // struct sockaddr_in

#include <arpa/inet.h>   // htonl()
//#include <string>
//#include <strings.h>  // bzero()

// #include <sys/types.h>

#define MAXLINE 3000 // Max word received from FLIC: 5000 words
//#define bzero(s, n) memset((s), 0, (n))

void dg_echo(int sockfd, struct sockaddr *pcliaddr, socklen_t clilen) {
  
  int n;
  socklen_t len;
  uint16_t mesg[MAXLINE] = {0};  // uint16_t -> unsigned short int

  int i;

  //  printf("print something before the infinite for loop\n");
  
  len = clilen;
  n = recvfrom(sockfd, mesg, MAXLINE, 0, pcliaddr, &len);
  
  //    fputs(mesg, stdout);
  /*
  printf("Received--->\n");
  for (i = 0; i < 1500 ; ++i )
    printf("----------->%04x\n", mesg[i]);
 

  printf("******************end of UDP packet**********************\n");
  */
  /*
  // print data to file
  FILE *f = fopen("vector_from_flic.txt", "w");
  if (f == NULL)
  {
    printf("Error opening file!\n");
    exit(1);
  }

  // print data to file 
  for (i = 0; i < 1500; ++i)
    fprintf(f, "%04x\n", mesg[i]);

  fclose(f);
  */
      
  //  sendto(sockfd, mesg, n, 0, pcliaddr, len);
}




int main(int argc, char **argv) {
  int sockfd;
  // struct sockaddr_in is from <netinet/in.h>
  struct sockaddr_in servaddr, cliaddr;

  int udp_packet_counter = 0;

  
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);

  //  bzero(&servaddr, sizeof(servaddr));

  memset(&servaddr, 0, sizeof(servaddr));
  servaddr.sin_family = AF_INET;

  // #include <arpa/inet.h>  The htonl() function converts
  // the unsigned integer hostlong from host byte order to network byte order.
  servaddr.sin_addr.s_addr = htonl(INADDR_ANY);

  // #include <arpa/inet.h> The htons() function converts
  // the unsigned short integer hostshort from host byte order
  // to network byte order.
  servaddr.sin_port = htons(50001);  // SERV_PORT);

  // bind socket with address
  bind(sockfd, (struct sockaddr *) &servaddr, sizeof(servaddr));



  if (argc != 2) {
    perror("usage: udpserv <IPaddress> -- give client IP address");
    exit(1);
  }



  memset(&cliaddr, 0, sizeof(cliaddr));

  cliaddr.sin_family = AF_INET;
  // convert IPv4 and IPv6 addresses from text to binary form
  inet_pton(AF_INET, argv[1], &cliaddr.sin_addr);
  cliaddr.sin_port = htons(50000);   // CLI_PORT);


  printf("------> a mark before dg_echo() \n");
  
  while(1) {
    ++ udp_packet_counter;
    if (udp_packet_counter%1000 == 0)
      printf("udp_packet_counter: %d\n", udp_packet_counter);
    dg_echo(sockfd, (struct sockaddr *) &cliaddr, sizeof(cliaddr));
  }

  return 0;
}
