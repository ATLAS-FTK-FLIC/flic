AssemblyHeader::AssemblyHeader() {
}

uint16_t AssemblyHeader::getLevel1ID() const {
  return level1ID;
}

uint16_t AssemblyHeader::getNOfWordsFollowingThisWord() const {
  return nOfWordsFollowingThisWord; 
}

uint16_t AssemblyHeader::getBitmask() const {
  return bitmask; 
}

uint16_t AssemblyHeader::getFragmentID0123() const {
  return fragmentID0123; 
}

uint16_t AssemblyHeader::getFragmentID4567() const {
  return fragmentID4567;
}

AssemblyHeader & AssemblyHeader::setLevel1ID(uint16_t l1ID) {
  level1ID = l1ID;

  return *this;
}


AssemblyHeader & AssemblyHeader::setNOfWordsFollowingThisWord(uint16_t nWordsFollowing) {
  nOfWordsFollowingThisWord = nWordsFollowing;

  return *this;
}


AssemblyHeader & AssemblyHeader::setBitmask(uint16_t mask) {
  bitmask = mask;

  return *this;
}


AssemblyHeader & AssemblyHeader::setFragmentID0123(uint16_t fragID0123) {
  fragmentID0123 = fragID0123;

  return *this;
}


AssemblyHeader & AssemblyHeader::setFragmentID4567(uint16_t fragID4567) {
  fragmentID4567 = fragID4567;

  return *this;
}
