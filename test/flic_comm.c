#include <iostream>
#include <sstream>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <netdb.h>

#include "flic_comm.h"

bool IsIPInRange(const char* hostIP, const char* flicIP);
uint32_t IPtoUInt(const char* ip);

flic_comm::flic_comm(const char* flicIP,const char* flicPort){
  //Determine Host IP that will communicate with FLIC.
  struct ifaddrs * ifAddrStruct=NULL;
  struct ifaddrs * ifa=NULL;
  void * tmpAddrPtr=NULL;
  char* hostIP=NULL;
  
  getifaddrs(&ifAddrStruct);
  
  for (ifa = ifAddrStruct; ifa != NULL; ifa = ifa->ifa_next) {
    if (!ifa->ifa_addr) {
      continue;
    }
    if (ifa->ifa_addr->sa_family == AF_INET) { // check it is IP4
      // is a valid IP4 Address
      tmpAddrPtr=&((struct sockaddr_in *)ifa->ifa_addr)->sin_addr;
      char addressBuffer[INET_ADDRSTRLEN];
      inet_ntop(AF_INET, tmpAddrPtr, addressBuffer, INET_ADDRSTRLEN);
      //printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer); 
      if(IsIPInRange(addressBuffer,flicIP)) { //Match IPv4s
	hostIP=addressBuffer;
	break;   
      }
    } 
    else if (ifa->ifa_addr->sa_family == AF_INET6) { // check it is IP6
      // is a valid IP6 Address
      tmpAddrPtr=&((struct sockaddr_in6 *)ifa->ifa_addr)->sin6_addr;
      char addressBuffer[INET6_ADDRSTRLEN];
      inet_ntop(AF_INET6, tmpAddrPtr, addressBuffer, INET6_ADDRSTRLEN);
      //printf("%s IP Address %s\n", ifa->ifa_name, addressBuffer); 
      if(IsIPInRange(addressBuffer,flicIP)){   //Match IPv6s
	hostIP=addressBuffer;
	break;   
      }
    } 
  }
  
  //Check if match found...
  if(hostIP!=NULL){
    printf("Connecting to FLIC from %s\n",hostIP);
  }
  else{
    error("ERROR Host has no IP on FLIC subnet.\n Error results in");
  }
  if (ifAddrStruct!=NULL) freeifaddrs(ifAddrStruct);

  //Open socket to FLIC:
  std::stringstream ssport;
  ssport << flicPort;
  ssport >> portno;
  broadcast=1;
  
  sockfd = socket(AF_INET, SOCK_DGRAM, 0);
  setsockopt(sockfd, SOL_SOCKET, SO_BROADCAST,
	     &broadcast, sizeof broadcast);

  if (sockfd < 0) 
    error("ERROR opening socket");
  
  bzero((char *) &flic_addr, sizeof(flic_addr));
  flic_addr.sin_family = AF_INET;
  flic_addr.sin_addr.s_addr = INADDR_ANY;
  flic_addr.sin_port = htons(portno);
  if(bind(sockfd,(struct sockaddr *) &flic_addr, sizeof(flic_addr)) < 0)
    error("ERROR on binding");
  
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_DGRAM;
  hints.ai_flags = 0;
  hints.ai_protocol = 0;
  
  status = getaddrinfo(hostIP,flicPort, &hints, &result);

  for(i_res=result;i_res != NULL; i_res = i_res->ai_next){
    sfd = socket(i_res->ai_family, i_res->ai_socktype, i_res->ai_protocol);
    if(sfd==-1)
      continue;
    status = getaddrinfo(flicIP,flicPort, &hints, &result);
    i_res=result;
    if(connect(sfd, i_res->ai_addr, i_res->ai_addrlen) != -1)
      break;
  }//for(i_res)
  
  if(i_res==NULL)
    error("Could not connect.");
  
  std::cout<<"Communication with FLIC established."<<std::endl;
};

void flic_comm::error(const char *msg){
  perror(msg);
  exit(0);
};

int flic_comm::singlewrite(ushort target, ushort command, uint address, ushort value,ushort status=0){

  tx.header.target  = target;
  tx.header.command = command;
  tx.header.address = address;
  tx.header.size    = 1;
  tx.header.status  = status;
  tx.data.word[0]   = value;
  ushort txLength   = sizeof(UDP_Header)+sizeof((ushort)value);
  /*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%04x ", tx.data.word[0]  );
  printf("%012x ", txLength);
  printf("\n");
  */
  n = write(sfd,&tx,txLength);
  
  if(n<0)
    error("ERROR writing to FLIC.");
  
  n = recv(sockfd,&rx,sizeof rx, 0);

  if (n <0)
    error("ERROR reading from FLIC.");
  
  //Pause to keep from overloading system.
  usleep(100000);

  return n;
};

int flic_comm::singlewrite(ushort target, ushort command, uint address, ushort value){

  tx.header.target  = target;
  tx.header.command = command;
  tx.header.address = address;
  tx.header.size    = 1;
  tx.header.status  = 0;
  tx.data.word[0]   = value;
  ushort txLength   = sizeof(UDP_Header)+sizeof((ushort)value);
  /*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%04x ", tx.data.word[0]  );
  printf("%012x ", txLength);
  printf("\n");
  */
  n = write(sfd,&tx,txLength);
  
  if(n<0)
    error("ERROR writing to FLIC.");
  
  n = recv(sockfd,&rx,sizeof rx, 0);

  if (n <0)
    error("ERROR reading from FLIC.");
  
  //Pause to keep from overloading system.
  usleep(100000);
  
  return n;
};

int flic_comm::singleread(ushort target, ushort command, uint address, ushort* value,ushort status=0){

  tx.header.target  = target;
  tx.header.command = command;
  tx.header.address = address;
  tx.header.size    = 1;
  tx.header.status  = status;
  tx.data.word[0]   = (*value);
  ushort txLength   = sizeof(UDP_Header)+sizeof((ushort)0x0001);
  /*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%04x ", tx.data.word[0]  );
  printf("%012x ", txLength);
  printf("\n\n");
  */
  n = write(sfd,&tx,txLength);
  
  if(n<0)
    error("ERROR writing to FLIC.");
  
  n = recv(sockfd,&rx,sizeof rx, 0);

  if (n <0){
    error("ERROR reading from FLIC.");
    return -1;
  }
  
  (*value)=rx.data.word[0];
  return n;
};

int flic_comm::singleread(ushort target, ushort command, uint address, ushort* value){

  tx.header.target  = target;
  tx.header.command = command;
  tx.header.address = address;
  tx.header.size    = 1;
  tx.header.status  = 0;
  tx.data.word[0]   = (*value);
  ushort txLength   = sizeof(UDP_Header)+sizeof((ushort)0x0001);
  /*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%04x ", tx.data.word[0]  );
  printf("%012x ", txLength);
  printf("\n\n");
  */
  n = write(sfd,&tx,txLength);
  
  if(n<0)
    error("ERROR writing to FLIC.");
  
  n = recv(sockfd,&rx,sizeof rx, 0);

  if (n <0){
    error("ERROR reading from FLIC.");
    return -1;
  }
  
  (*value)=rx.data.word[0];
  return n;
}


int flic_comm::blockwrite(ushort target, ushort command, uint address, UDP_Data& block){
  tx.header.target  = target;
  tx.header.command = command;
  tx.header.address = address;
  tx.header.size    = 256;
/*  UDP_Data data;
  for (int i = 0;i<512;i++){
    data.word[i] = block[i];
  }
*/
  for(int iw=0;iw<256;iw++)
    tx.data.word[iw] = block.word[iw];
  tx.header.status  = 0;
  ushort txLength   = sizeof(UDP_Header)+(sizeof(ushort)*256);

/*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%0512x ", tx.data.word[0]  );
  printf("%4x ", txLength);
  printf("\n\n");
*/
  n = write(sfd,&tx,txLength);

  if(n<0)
    error("ERROR writing to FLIC.");

  n = recv(sockfd,&rx,sizeof rx, 0);

  if (n <0)
    error("ERROR reading from FLIC.");
  //printf("%8x ", tx.data.word[0]  );
  return n;
}

int flic_comm::blockread(ushort target, ushort command, uint address, ushort* data){
  //printf(" flic_comm::blockread"  );
  tx.header.target  = target;
  tx.header.command = command;
  tx.header.address = address;
  tx.header.size    = 256;
  tx.header.status  = 0;
  ushort txLength   = sizeof(UDP_Header);

  /*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%0512x ", tx.data.word[0]  );
  printf("%4x ", txLength);
  printf("\n\n");
  */
  printf(" flic_comm::blockread"  );
  n = write(sfd,&tx,txLength);
  printf(" flic_comm::blockread: write"  );
  if(n<0)
    error("ERROR writing to FLIC.");

  n = recv(sockfd,&rx,sizeof rx, 0);
  printf(" flic_comm::blockread: read"  );

  if (n <0)
    error("ERROR reading from FLIC.");
  for(int i=0;i<rx.header.size;i++)data[i] = rx.data.word[i];
  return n;
//  return 0;
}

int flic_comm::adcScan(ushort target, ushort command, uint address, ushort* value){
  tx.header.target	= target;
  tx.header.command	= command;
  tx.header.address	= address;
  tx.header.size	= 1;
  tx.header.status	= 0;
//  tx.data.word[0]	= value;				//not used; function has no argument
  ushort txLength   = sizeof(UDP_Header);

  /*
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%0512x ", tx.data.word[0]  );
  printf("%4x ", txLength);
  printf("\n\n");
  */
  printf(" flic_comm::adcScan"  );
  n = write(sfd,&tx,txLength);
  printf(" flic_comm::adcScan: write"  );
  if(n<0)
    error("ERROR writing to FLIC.");

  n = recv(sockfd,&rx,sizeof rx, 0);
  printf(" flic_comm::adcScan: read"  );

  if (n <0)
    error("ERROR reading from FLIC.");
  for(int i=0;i<rx.header.size;i++)value[i] = rx.data.word[i];
  return n;
	
}

int flic_comm::adcSingle (ushort target, ushort command, uint address, ushort* value)
{
  tx.header.target	= target;
  tx.header.command	= command;
  tx.header.address	= address;
  tx.header.size		= 1;
  tx.header.status	= 0;
  tx.data.word[0]		= *value;				//not used; function has no argument

  ushort txLength   = sizeof(UDP_Header);//+sizeof((ushort)0x0001);
  
  printf("%02x ", tx.header.target );
  printf("%04x ", tx.header.command);
  printf("%04x ", tx.header.address);
  printf("%04x ", tx.header.size   );
  printf("%04x ", tx.data.word[0]  );
  printf("%012x ", txLength);
  printf("\n\n");
  
  n = write(sfd,&tx,txLength);
  if(n<0)
    error("ERROR writing to FLIC.");

  n = recv(sockfd,&rx,sizeof rx, 0);

  if (n <0){
    error("ERROR reading from FLIC.");
    return -1;
  }

  (*value)=rx.data.word[0];
  return n;
	
}

bool IsIPInRange(const char* hostIP, const char* flicIP){
  const char* mask="255.255.255.0";
  uint32_t host_addr = IPtoUInt(hostIP);
  uint32_t flic_addr = IPtoUInt(flicIP);
  uint32_t mask_addr = IPtoUInt(mask);
  
  uint32_t net_lower = (flic_addr & mask_addr);
  uint32_t net_upper = (net_lower | (~mask_addr));
  
  if (host_addr >= net_lower &&
      host_addr <= net_upper)
    return true;
  return false;
}

uint32_t IPtoUInt(const char* ip){ 
  int a, b, c, d;
  uint32_t addr = 0;

  if (sscanf(ip, "%d.%d.%d.%d", &a, &b, &c, &d) != 4)
    return 0;
  
  addr = a << 24;
  addr |= b << 16;
  addr |= c << 8;
  addr |= d;
  return addr;
}
