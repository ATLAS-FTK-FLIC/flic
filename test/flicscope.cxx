#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <cmath>
#include <sys/stat.h>
#include <fstream>

#include "flic_comm.h"
#include "FLIC.h"

int PicPowerOn(flic_comm flictalk);
int PicPowerOff(flic_comm flictalk);
int ProgramAll(flic_comm flictalk);
int SERDESReset(flic_comm flictalk);
int SMResets(flic_comm flictalk);
int ClearFIFOFullFlags(flic_comm flictalk);
int Idle(flic_comm flictalk);
int SendEvent(flic_comm flictalk);  
int SendMany(flic_comm flictalk);  
int ResetSSB(flic_comm flictalk);
int SetupFLIC(flic_comm flictalk);
int WriteRegister(flic_comm flictalk);
int ReadRegister(flic_comm flictalk);
int DownloadFirmware(flic_comm flictalk);
int DownloadSRAM(flic_comm flictalk);
int PulseGTX112Reset(flic_comm flictalk);
int ResetSLink(flic_comm flictalk);
int ChangeRunParam(flic_comm flictalk);
int ChangeTrackParam(flic_comm flictalk);
int ChangeDelayParam(flic_comm flictalk);
int Sync(flic_comm flictalk);

int main(){
  int bytesent=0;
  int option=1;
  
  flic_comm flic1("10.153.37.18","50000"); //USA15
  //...//flic_comm flic1("10.193.32.17","50000"); //Lab4

  while(option!=0){
    printf("\n");
    printf("Select an option:\n");
    printf("   0 Exit                                                   \n");
    printf("   1 Power On                    100 Power Off              \n");
    printf("============================================================\n");
    printf("   2 Program All FPGAs             3 Idle Board             \n");
    printf("   4 Setup One Channel Proc.       5 Send one event to sync \n");//rwang
    printf("   6 Reset SSBe                    20 Send one event        \n");//rwang
    printf("============================================================\n");
    printf("   7 Read a register               8 Write a register       \n");
    printf("   9 Download Firmware                                      \n");
    printf("============================================================\n");
    printf("  11 Pulse flow control to SSB    12 Reset S-Link           \n");
    printf("  13 Change Emulation Parameters  14 Run Continuously       \n");
    printf("Your choice: ");
    scanf("%d",&option);
    if (option == 1) bytesent+=PicPowerOn(flic1);
    if (option == 100) bytesent+=PicPowerOff(flic1);
    if (option == 2) bytesent+=ProgramAll(flic1);
    if (option == 3) bytesent+=Idle(flic1);
    if (option == 4) bytesent+=SetupFLIC(flic1);
    if (option == 5) bytesent+=Sync(flic1);//rwang
    if (option == 6) bytesent+=ResetSSB(flic1);
    if (option == 7) bytesent+=ReadRegister(flic1);
    if (option == 8) bytesent+=WriteRegister(flic1);
    if (option == 9) bytesent+=DownloadFirmware(flic1);
    if (option == 11) bytesent+=PulseGTX112Reset(flic1);
    if (option == 12) bytesent+=ResetSLink(flic1);
    if (option == 13) bytesent+=ChangeRunParam(flic1);
    if (option == 14) bytesent+=SendMany(flic1);
    if (option == 20) bytesent+=SendEvent(flic1);//rwang
    //option=0;//One time!
  }//while option
  
  flic1.closesockets();
  return bytesent;
}

//Function to send power to the board
int PicPowerOn(flic_comm flictalk){
  return flictalk.singlewrite(FLIC::targetPic,FLIC::cmdWrite,FLIC::PIC::picReg_Command, 0x0001, FLIC::statusNoError);
};

//Function to turn off power to the board
int PicPowerOff(flic_comm flictalk){
  return flictalk.singlewrite(FLIC::targetPic,FLIC::cmdWrite,FLIC::PIC::picReg_Command, 0x0000, FLIC::statusNoError);
};
//Function to program the FPGAs from on board memory
int ProgramAll(flic_comm flictalk){
  int bytes=0;
  const int nDEST=4;
  int isDone=0;
  ushort status[nDEST];//number of things being programmed for now just FPGA's  
  
  //Set control
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdWrite,FLIC::AUX::auxControlReg_Control0, 0x0004);
  
  //Start programming
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdSet,FLIC::AUX::auxControlReg_Control0, 0x000A);
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdSet,FLIC::AUX::auxPulseReg_Pulse0, 0x0001);
  
  //Check if FPGA firmware has been loaded.
  while(isDone < nDEST){
    usleep(100000);
    isDone=0;
    for(int i =0;i<nDEST;i++){
      bytes+=flictalk.singleread(FLIC::targetMgmtFpga,FLIC::cmdRead,0x108+i,&status[i]);
      if((status[i] & (1<<0)))
  	isDone++;
    }
  }//end while
  
  //Return control
  bytes+=flictalk.singlewrite(FLIC::targetMgmtFpga,FLIC::cmdClear,FLIC::AUX::auxControlReg_Control0, 0x000A, FLIC::statusNoError);

  return bytes;
}

int SERDESReset(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x2,0x60);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x2,0x6060);  
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x200,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0xf0);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x4444);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x8888);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x200,0x4444);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0x00);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x3,0xf0);  
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x4444);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x8888);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x208,0x4444);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x2,0x6060);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdClear,0x2,0x60);
  
  return bytes;
}

int SMResets(flic_comm flictalk){
  int bytes=0;
  //Pulse reset of all SMs on U1.
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x1f,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x1f,0x0000);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xffff);
  //Pulse reset of SSB Emulator or U2
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201,0xf);
  //same as GTX112 reset. Should be done. Here I reset the PRBS machine...
  return bytes;
}

int PulseGTX112Reset(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x2,0x60);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x2,0x60);
  return bytes;
}

int ClearFIFOFullFlags(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x20f,0xD0F0);
  return bytes;
}

int Idle(flic_comm flictalk){
  int bytes=0;
  //Optionally check if board is programmed? By reading addresses from AUX and looking at bit 0.
  bytes+=SERDESReset(flictalk);
  //bytes+=SetupFLIC(flictalk);
  //bytes+=SMResets(flictalk);
  return bytes;
}

int SendEvent(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201, 0x1000 );//rwang f000
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201, 0x2000 );//rwang f000
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201, 0x4000 );//rwang f000
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201, 0x8000 );//rwang f000
  return bytes;
}
int SendMany(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x1f,0xf00);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201, 0xf000 );//rwang f000
  return bytes;
}
  
int ResetSSB(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201,0xff);//rwang f
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdSet,0x1f,0xf000);
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x201,0xf);
  return bytes;
}


//Setup state machines for default running conditions.
//Default is defined as ch 3 of U2 sends PRBS data
//to ch 3 of U1 which does the SRAM lookup and mergeing 
//before fires it out the RTM on ch 3 to a ROS. (9/5/14)
int SetupFLIC(flic_comm flictalk){
  int bytes=0;
  
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000004, 0x0000);
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x00000004, 0x0000);
  
  // At this point U1 should be sending PRBS to U2 over the front SFP.  U2 should be sending commas to U1 over the front SFP.
  // At this point U1 is also sending PRBS out the RTM over the SLINK SFPs.  This should be changed to SLINK format for all links.
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000003, 0x00F0);
  
  // Now we do step 6 of the handwritten script.
  // Set SRAM access to state machine (bit 3 of GENERAL_CONTROL clear) in U1 (should already be so)
  // Enable Core Crate receiver machines (set bit 1 of GENERAL_CONTROL) in U1
  // Enable data merge machines (set bit 4 of GENERAL_CONTROL) in U1
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000002, 0x0012);
  bytes+=SMResets(flictalk);
  
  //Hit all Subsection Resets in U1, in order
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x8000);//reset the GTX116 FIFO reader
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x4000);//reset the GTX116 data FIFO
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x2000);//reset the SLINK primitives
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x1000);//reset the merge machines
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x0300);//reset the SRAM machines
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x00F0);//reset the CCMUX logic
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x000F);//reset the core crate receivers
  //
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x4444);//reset the GTX116 TLK wrappers
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x8888);//reset the GTX116 hola core logic
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x4444);//reset the GTX116 TLK wrappers
  
  // now enable the SLINK reset state machine to go
  bytes+=ResetSLink(flictalk);
  //Set all ILA tag bits in U1, GTX 112
  //bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000201, 0x0F00);	
  
  //Now switch SRAM control back to state machine by setting bit 3 of GENERAL_CONTROL register
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000002, 0x0008);	//set bit 3
  
  //set the SSB emulator in U2 to have status words of 0xDDDD and 0xEEEE
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0000001D, 0xDDDD);	
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x0000001E, 0xEEEE);	
  
  //as last steps, set the FLIC status words in U1 to 0x1515 and 0x1616, respectively
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000015, 0x1515);	
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000016, 0x1616);	
  //Make sure all FIFOs are empty.
  bytes+=ClearFIFOFullFlags(flictalk);

  // at this point we should be ready to push the first event to synchronize
  //reset SSB SM
  //bytes+=ResetSSB(flictalk);
  //bytes+=SendEvent(flictalk);
  //bytes+=ResetSSB(flictalk);
  
  //READY TO SEND DATA.
  return bytes;
}

int WriteRegister(flic_comm flictalk){
  int target=-1;
  int cmd=-1;
  unsigned int reg=0xffff;
  unsigned int value=0xffff;
  
  printf("\n***CAUTION*** There is no safety net beyond this point.\n\n");
  printf("Enter target:\n");
  printf("   %d  FPGA U1               %d  FPGA U2         \n",FLIC::targetFpgaU1,FLIC::targetFpgaU2);
  printf("   %d  FPGA U3               %d  FPGA U4         \n",FLIC::targetFpgaU3,FLIC::targetFpgaU4);
  printf("   %d  Flash 1               %d  Flash 2         \n",FLIC::targetFlash1,FLIC::targetFlash2);
  printf("   %d  Flash 3               %d  Flash 4         \n",FLIC::targetFlash3,FLIC::targetFlash4);
  printf("   %d  SRAM U1 1             %d  SRAM U1 2       \n",FLIC::targetSram1Fpga1,FLIC::targetSram2Fpga1);
  printf("   %d  SRAM U1 3             %d  SRAM U1 4	   \n",FLIC::targetSram3Fpga1,FLIC::targetSram4Fpga1);
  printf("   %d  SRAM U2 1             %d  SRAM U2 2	   \n",FLIC::targetSram1Fpga2,FLIC::targetSram2Fpga2);
  printf("   %d  SRAM U2 3             %d  SRAM U2 4       \n",FLIC::targetSram3Fpga2,FLIC::targetSram4Fpga2);
  printf("   %d  Mgmt FPGA             %d  Pic             \n",FLIC::targetMgmtFpga,FLIC::targetPic);
  printf("Target choice: ");
  scanf("%d",&target);

  printf("Enter command:\n");
  printf("   %d  Write                 %d  Set             \n",FLIC::cmdWrite,FLIC::cmdSet);
  printf("   %d  Clear                 %d  Erase           \n",FLIC::cmdClear,FLIC::cmdErase);
  printf("Command choice: ");
  scanf("%d",&cmd);

  printf("Enter register [hex]: ");
  scanf("%x",&reg);

  printf("Enter value/mask [hex]: ");
  scanf("%x",&value);

  
  return flictalk.singlewrite(target,cmd,reg, value, FLIC::statusNoError);
}

int ReadRegister(flic_comm flictalk){
  int bytes=0;
  int target=-1;
  unsigned int reg=0xffff;
  ushort value;
  
  printf("Enter target:\n");
  printf("   %d  FPGA U1               %d  FPGA U2         \n",FLIC::targetFpgaU1,FLIC::targetFpgaU2);
  printf("   %d  FPGA U3               %d  FPGA U4         \n",FLIC::targetFpgaU3,FLIC::targetFpgaU4);
  printf("   %d  Flash 1               %d  Flash 2         \n",FLIC::targetFlash1,FLIC::targetFlash2);
  printf("   %d  Flash 3               %d  Flash 4         \n",FLIC::targetFlash3,FLIC::targetFlash4);
  printf("   %d  SRAM U1 1             %d  SRAM U1 2       \n",FLIC::targetSram1Fpga1,FLIC::targetSram2Fpga1);
  printf("   %d  SRAM U1 3             %d  SRAM U1 4	   \n",FLIC::targetSram3Fpga1,FLIC::targetSram4Fpga1);
  printf("   %d  SRAM U2 1             %d  SRAM U2 2	   \n",FLIC::targetSram1Fpga2,FLIC::targetSram2Fpga2);
  printf("   %d  SRAM U2 3             %d  SRAM U2 4	   \n",FLIC::targetSram3Fpga2,FLIC::targetSram4Fpga2);
  printf("   %d  Mgmt FPGA             %d  Pic             \n",FLIC::targetMgmtFpga,FLIC::targetPic);
  printf("Target choice: ");
  scanf("%d",&target);

  printf("Enter register [hex]: ");
  scanf("%x",&reg);
  bytes = flictalk.singleread(target, FLIC::cmdRead, reg, &value);
  printf("\nRegister %x has value: %x\n",reg,value);
  return bytes;
}

int DownloadFirmware(flic_comm flictalk){
  int bytes=0;
  int bytesread=0;
  int target=-1;
  char filename[256];
  UDP_Data data;
  uint address = 0;
  int j=0;
  int fileEnd=0;
  char buf[512];
  unsigned char buf1[512];
  struct stat results;

  printf("Enter Flash Area:\n");
  printf("   %d  FPGA U1               %d  FPGA U2         \n",FLIC::targetFlash1,FLIC::targetFlash2);
  printf("   %d  FPGA U3               %d  FPGA U4         \n",FLIC::targetFlash3,FLIC::targetFlash4);
  printf("Target choice: ");
  scanf("%d",&target);

  printf("Firmware File [*.bin]: ");
  scanf("%s",&filename[0]);
  
  //Check file size is not too big
  std::ifstream file(filename,std::ios::in | std::ios::binary);
//  std::ifstream file(filename,std::ifstream::binary);
  if (stat(filename, &results) == 0){
    if((uint)results.st_size >
       FLIC::Flash::flash_region[FLIC::regionFpga].blocks * FLIC::Flash::FLASH_BLOCK_SIZE  ){
      printf("FIRMWARE IS TOO BIG! %d vs %d",(uint)results.st_size,FLIC::Flash::flash_region[FLIC::regionFpga].blocks * FLIC::Flash::FLASH_BLOCK_SIZE );
      return 0;
    }
    //Erase what is currently in the Flash area.
    printf("\nErasing ");
    address=FLIC::Flash::flash_region[FLIC::regionFpga].start;
    for(uint iblk=0;iblk<FLIC::Flash::flash_region[FLIC::regionFpga].blocks;iblk++){
      printf(". "); fflush(stdout);
      flictalk.singlewrite(target, FLIC::cmdErase, address, 0x0000);
      usleep(25000);
      address+=FLIC::Flash::FLASH_BLOCK_WORDS;//FLASH_BLOCK_WORDS
    }
    printf("done.\n\n");


    address=FLIC::Flash::flash_region[FLIC::regionFpga].start;
    do {
      // Attempt to read a full page from the file
      //printf("\nread file\n");
      file.read(buf,512);
      for(int i = 0;i<256;i++){
        buf1[2*i]=buf[2*i];
        buf1[2*i+1]=buf[2*i+1];
        //printf(" %x%x",buf1[2*i],buf1[2*i+1]);
        data.word[i]=buf1[2*i+1]*0x100+buf1[2*i];
        //printf(" %04x",data.word[i]);
      }
      bytesread=file.gcount();

      if (bytesread != 512 ) {
        // Fill any remaining bytes in page with zeros
        for (j = bytesread; j < 512/2 ; j++) {
          data.word[j] = 0;
        }
        fileEnd = 1;
      }
      bytes+=flictalk.blockwrite(target,FLIC::cmdArrayWrite, address, data);
      usleep(20000);

      address += 256;
      //printf("\rProgramming %d %% at %x value %04x", (100*address)/(FLIC::Flash::flash_region[FLIC::regionFpga].stop),address,data.word[0]);fflush(stdout);
      printf("\rProgramming %d %%", (100*address)/(FLIC::Flash::flash_region[FLIC::regionFpga].stop));fflush(stdout);
    }
    while ((address < FLIC::Flash::flash_region[FLIC::regionFpga].stop) && (fileEnd == 0));

    printf("\nWrote %x of %x available bytes.",address,FLIC::Flash::flash_region[FLIC::regionFpga].stop);
    file.close();
  }
  else
    return 0;

  return bytes;
}

int DownloadSRAM(flic_comm flictalk){
  int bytes=0;
  return bytes;
}
int ResetSLink(flic_comm flictalk){
  int bytes=0;
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x00000003, 0x0F00);//clear the GO bits to the reset machines
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000209, 0x0F00);//request the reset machines to re-arm
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000003, 0x0F00);//set the GO bits to the reset machines
  return bytes;
}

int Sync(flic_comm flictalk){
  int bytes=0;
//  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdClear,0x00000003, 0x0F00);//clear the GO bits to the reset machines
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x00000209, 0x0F00);//request the reset machines to re-arm
  bytes+=SendEvent(flictalk);
  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdWrite,0x0000020F, 0x4000);//reset GTX116 event FIFO
//  bytes+=flictalk.singlewrite(FLIC::targetFpgaU1,FLIC::cmdSet,0x00000003, 0x0F00);//set the GO bits to the reset machines
  return bytes;
}

int ChangeRunParam(flic_comm flictalk){
  int bytes=0;
  int option=1;
  
  while(option!=0){
    printf("\n");
    printf("Select an option:\n");
    printf("   0 Return to main menu\n");
    printf("============================================================\n");  
    printf("   1 Tracks in record              2 Delay between records  \n");
    printf("============================================================\n");
    printf("Your choice: ");
    scanf("%d",&option);
    if (option == 1) bytes+=ChangeTrackParam(flictalk);
    if (option == 2) bytes+=ChangeDelayParam(flictalk);
  }//while option
  return bytes;
}

int ChangeDelayParam(flic_comm flictalk){
  int bytes=0;
  int option=1;
  uint randRange=4;
  uint fixedDelay=12;
  unsigned int value=0x0;

  printf("\n");
  printf("Select an option:\n");
  printf("============================================================\n");  
  printf("   1 Random                        2 Fixed                  \n");
  printf("============================================================\n");
  printf("Your choice: ");
  scanf("%d",&option);
  
  if (option == 1){//RANDOM OPTIONS
    printf("============================================================\n");
    printf("Enter minimum delay [0-4096] : ");
    scanf("%d",&fixedDelay);
    printf("Enter range as power of two [1-7] : ");
    scanf("%d",&randRange);
    
    printf("============================================================\n");
    printf("FLIC SSBe will produce between %d and %d word delay per record. \n",fixedDelay,fixedDelay-2+(int)pow(2,randRange));
    
    //SEND HERE!
    value=randRange*0x1000;
    value+=fixedDelay;
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x018, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x019, value);
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x01A, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x01B, value);    

  }//RANDOM
  
  if (option == 2){//FIXED OPTIONS
    printf("============================================================\n");
    printf("Enter fixed delay [0-4096] : ");
    scanf("%d",&fixedDelay);
    printf("FLIC SSBe will produce %d word delay per record. \n",fixedDelay);
    value=0x8000;
    value+=fixedDelay;
    //SEND HERE
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x018, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x019, value);
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x01A, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x01B, value);    

  }
  

  return bytes;
}

int ChangeTrackParam(flic_comm flictalk){
  int bytes=0;
  int option=1;
  uint randRange=4;
  uint fixedTrack=12;
  unsigned int value=0x0;

  printf("\n");
  printf("Select an option:\n");
  printf("============================================================\n");  
  printf("   1 Random                        2 Fixed                  \n");
  printf("============================================================\n");
  printf("Your choice: ");
  scanf("%d",&option);
  
  if (option == 1){//RANDOM OPTIONS
    printf("============================================================\n");
    printf("Enter minimum number of tracks [1-16] : ");
    scanf("%d",&fixedTrack);
    printf("Enter range as power of two [1-7] : ");
    scanf("%d",&randRange);
    
    printf("============================================================\n");
    printf("FLIC SSBe will produce between %d and %d tracks per record. \n",fixedTrack,fixedTrack-2+(int)pow(2,randRange));
    
    //SEND HERE!
    value=randRange*0x1000;
    value+=fixedTrack*0x100;
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x020, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x021, value);
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x022, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x023, value);    

  }//RANDOM
  
  if (option == 2){//FIXED OPTIONS
    printf("============================================================\n");
    printf("Enter fixed number of tracks [1-256] : ");
    scanf("%d",&fixedTrack);
    fixedTrack--;
    printf("FLIC SSBe will produce %d tracks per record. \n",fixedTrack+1);
    value=0x8000;
    value+=fixedTrack;
    //SEND HERE
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x020, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x021, value);
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x022, value);    
    bytes+=flictalk.singlewrite(FLIC::targetFpgaU2,FLIC::cmdWrite,0x023, value);    

  }
  

  return bytes;
}
