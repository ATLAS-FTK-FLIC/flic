#!/bin/sh

file="FLIC.h"
path="/users/jlove/FTKwork/tmpCode/Software/FlicWare"
flicheader="FlicDiag.h"
picheader="Pic.h"
auxheader="AuxFpga.h"
u1header="U1Fpga.h"
u2header="U2Fpga.h"
u3header="U3Fpga.h"
u4header="U4Fpga.h"


if [ -a $file ]
then
    mv ${file} ${file}"_prev"
fi

touch $file

#
# Make FLIC.h
#
cat << EOF | sed 's/\^M//g' >> $file


namespace FLIC{
`awk  '/enum/,/\}\;/' ${path}/${flicheader}`
  namespace PIC{
`awk  '/enum/,/\}\;/' ${path}/${picheader}`
}
  namespace AUX{
    `awk  '/enum/,/\}\;/' ${path}/${auxheader}`
}
  namespace U1{
    `awk  '/enum/,/\}\;/' ${path}/${u1header}`
}
  namespace U2{
    `awk  '/enum/,/\}\;/' ${path}/${u2header}`
}
  namespace U3{
    `awk  '/enum/,/\}\;/' ${path}/${u3header}`
}
  namespace U4{
    `awk  '/enum/,/\}\;/' ${path}/${u4header}`
}
}
EOF