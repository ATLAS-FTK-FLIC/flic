0xee12
0x34ee <-- Start of Header Marker
0x0000
0x0009 <-- Header size - fixed
0x0301
0x0000 <-- Format version number - formed within FLIC from firmware
0x0000
0x007f <-- Source identifier - fixed
0x0003
0x652e <-- 0 | RunNumber[30:0]
0x0008
0x66e8 <-- Extended level 1 ID
0x0000
0x01e0 <-- Res[31:12] | BCID[11:0]
0x0000
0x0000 <-- Res[31:8] | L1TriggerType[7:0]
0x0000
0x0000 <-- Res[31:24] | DetectorEventType[23:16] | Res[15:4] | TIM[3:0]
0x0bda
0x2df6 <-- Res[3:0] | L | 0xbda | sector
0x0016
0x0fff <-- 
0x0000
0x0000 <-- RoadID[23:0]
0x4584
0x8018 <-- Track chi2[31:16] | Track d0[15:0]
0x823e
0x927b <-- Track z0[31:16] | Track coth[15:0]
0x2515
0x38a3 <-- Track phi0[31:16] | Track curv[15:0]
0x0000
0x0147 <-- Res[31:12] | Module L0
0x10e1
0x15f9 <-- IBL
0x0000
0x0271 <-- Res[31:12] | Module L1
0x1540
0x1428 <-- PL0b
0x0000
0x0412 <-- Res[31:12] | Module L2
0x22d7
0x25ec <-- PL1b
0x0000
0x0675 <-- Res[31:12] | Module L3
0x1880
0x2066 <-- PL2b
0x04a4
0x09a9 <-- SAx0
0x0470
0x09a8 <-- SSt0
0x005a
0x0d3a <-- SAx1
0x0077
0x0d3b <-- SSt1
0x024c
0x1175 <-- SAx2
0x025d
0x1174 <-- SSt2
0x0456
0x166c <-- SAx3
0x0490
0x166d <-- SSt3
0xe0da
0x0000 <-- E0DA (begin debug block) | Length of debug block | Indeterminate[3:0]
0xe0df
0x0000 <-- E0DF (end debug block)   | Length of debug block | Indeterminate[3:0]
0x0000
0x0000 <-- L1ID
0x0000
0x0001 <-- ErrorFlag
0x0000
0x0000 <-- Res
0x0000
0x0000 <-- Res
